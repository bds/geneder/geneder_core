#!/bin/bash -l
#SBATCH -J geneder:04:doc
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --time=0-00:01:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# I/Os and parameters
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/04/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/04-Prepare_datasets/

# Load configuration
source ../libs/conf/confSH.sh
create_variables ../Confs/datasets_config.yml
create_variables ../Confs/biomarkers_config.yml

# Clean start
rm -rf ${OUTPUT_FOLDER}results_summary.*

# Print header
echo '\documentclass[]{article}' > ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\usepackage{graphicx}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\title{GeneDER - step 04 - Prepare datasets}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\author{Leon-Charles Tranchevent}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\begin{document}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\maketitle' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\textsl{}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\begin{abstract}' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'This document summarizes the results of the step 04-Prepare\_datasets. Age distributions' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'for various sample categories are displayed for all datasets. Categories are defined' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'based on gender, disease status or both (see colour codes in the legend).' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'In addition, the profiles of relevant tissue biomarkers are shown.\\' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'Note: this document is automatically generated.' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\end{abstract}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\clearpage' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex

# Global analysis.
echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
echo '	\centering' >> ${OUTPUT_FOLDER}results_summary.tex
echo '	\includegraphics[scale=0.34]{'"$OUTPUT_FOLDER"'Average_age_differences.png}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '	\caption{Average age difference (y-axis, in years) for the various datasets (x-axis).' >> ${OUTPUT_FOLDER}results_summary.tex
echo '	(Green) Samples are grouped according to the disease status of the patients (PD / Control).' >> ${OUTPUT_FOLDER}results_summary.tex
echo '	(Orange) Samples are grouped according to the gender of the patients (Female / Male).' >> ${OUTPUT_FOLDER}results_summary.tex
echo '	}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex

# Per dataset analysis.
nbDatasets=${#datasets__dataset_name[@]}
for (( i=0; i<$nbDatasets; i++ ))
do
	datasetName=${datasets__dataset_name[$i]}
	datasetHasAge=${datasets__has_age[$i]}
	if [ "${datasetHasAge}" == "'TRUE'" ]
	then
		echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\centering' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"${datasetName}"'_agebias_wrt_gender.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"${datasetName}"'_agebias_wrt_disease_status.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"${datasetName}"'_agebias_wrt_gender_and_disease_status.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\caption{Distributions of patient age for dataset '"${datasetName}"' and different sample categories. Each' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	sample is represented by a single point, colours have different meanings on the different plots. (Left) age' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	distributions of females (green) and males (orange). (Center) age distributions of controls (green) and' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	PD patients (orange). (Right) Combined representations for female controls (green), female PD patients' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	(orange), male controls (violet), and male PD patients (pink). NOTE: boxplots should be interpreted with' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	caution due to the low number of samples per category.}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex
	fi
	echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	\centering' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"${datasetName}"'_VSN_category_bias.png}' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"${datasetName}"'_VSN_category_bias_F_Control.png}' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"${datasetName}"'_VSN_category_bias_M_Control.png}' >> ${OUTPUT_FOLDER}results_summary.tex
	if [ -f  ${OUTPUT_FOLDER}/${datasetName}_VSN_category_bias_F_PD.png ]
		then
			echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"${datasetName}"'_VSN_category_bias_F_PD.png}' >> ${OUTPUT_FOLDER}results_summary.tex
	fi
	echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"${datasetName}"'_VSN_category_bias_M_PD.png}' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	\caption{PCA plots for dataset '"${datasetName}"' (VSN data). Each' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	sample is represented by a single point, colours represent both gender and disease status.' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	From top to bottom: all categories have different colours, female controls are highlighted' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	male controls are highlighted, female patients are highlighted, male patients are highlighted.' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	(Left) PC1 versus PC2. (Center) PC1 versus PC3.' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	(Right) PC2 versus PC3.' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	}' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '' >> ${OUTPUT_FOLDER}results_summary.tex

	echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	\centering' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"${datasetName}"'_noVSN_category_bias.png}' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"${datasetName}"'_noVSN_category_bias_F_Control.png}' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"${datasetName}"'_noVSN_category_bias_M_Control.png}' >> ${OUTPUT_FOLDER}results_summary.tex
	if [ -f  ${OUTPUT_FOLDER}/${datasetName}_noVSN_category_bias_F_PD.png ]
		then
			echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"${datasetName}"'_noVSN_category_bias_F_PD.png}' >> ${OUTPUT_FOLDER}results_summary.tex
	fi
	echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"${datasetName}"'_noVSN_category_bias_M_PD.png}' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	\caption{PCA plots for dataset '"${datasetName}"' (noVSN data). Each' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	sample is represented by a single point, colours represent both gender and disease status.' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	From top to bottom: all categories have different colours, female controls are highlighted' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	male controls are highlighted, female patients are highlighted, male patients are highlighted.' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	(Left) PC1 versus PC2. (Center) PC1 versus PC3.' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	(Right) PC2 versus PC3.' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	}' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '' >> ${OUTPUT_FOLDER}results_summary.tex

	echo '\clearpage' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '' >> ${OUTPUT_FOLDER}results_summary.tex

done

# Print footer
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\end{document}' >> ${OUTPUT_FOLDER}results_summary.tex

# Compilation
pdflatex -synctex=1 -interaction=nonstopmode ${OUTPUT_FOLDER}results_summary.tex
mv results_summary.pdf ${OUTPUT_FOLDER}
rm results_summary*

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
