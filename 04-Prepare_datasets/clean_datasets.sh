#!/bin/bash -l
#SBATCH -J geneder:04:clean
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 2
#SBATCH --time=0-00:05:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/04/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/04-Prepare_datasets/

# Loading modules.
resif-load-swset-legacy
module load lang/R/3.6.2-foss-2019b-bare

# Load configuration
source ../libs/conf/confSH.sh
create_variables ../Confs/datasets_config.yml

# Actual cleaning job.
Rscript --vanilla ${CODE_FOLDER}clean_datasets.R > ${OUTPUT_FOLDER}clean_log.out 2> ${OUTPUT_FOLDER}clean_log.err

# Counting the number of samples per category and per dataset.
rm -rf ${OUTPUT_FOLDER}clinical_categories_summarized.tsv
nbDatasets=${#datasets__dataset_name[@]}
for (( i=0; i<$nbDatasets; i++ ))
do
	datasetName=${datasets__dataset_name[$i]}
	cut -f 2-3 ${OUTPUT_FOLDER}${datasetName}_clinical.tsv | grep -v Disease | sort | uniq -c | sed 's/^\s*//' | sed -r 's/\s/\t/g' | awk -v OFS="\t" -F"\t" '{print $3, $2, $1}' | sed -r 's/\t/./' | sed -r 's/^/'"${datasetName}"'\t/g' >> ${OUTPUT_FOLDER}clinical_categories_summarized.tsv
done

# Refine the counts.
Rscript --vanilla ${CODE_FOLDER}refine_counts.R > ${OUTPUT_FOLDER}counts_log.out 2> ${OUTPUT_FOLDER}counts_log.err

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
