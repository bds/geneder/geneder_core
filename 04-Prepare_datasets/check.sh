#!/bin/bash -l
#SBATCH -J geneder:04:check
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 2
#SBATCH --time=0-00:15:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/04/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/04-Prepare_datasets/

# Loading modules.
resif-load-swset-legacy
module load lang/R/3.6.2-foss-2019b-bare

# Actual jobs.
Rscript --vanilla ${CODE_FOLDER}check_clinical_balance.R       > ${OUTPUT_FOLDER}check_age_log.out 2> ${OUTPUT_FOLDER}check_age_log.err
Rscript --vanilla ${CODE_FOLDER}check_category_bias.R          > ${OUTPUT_FOLDER}check_cat_log.out 2> ${OUTPUT_FOLDER}check_cat_log.err
Rscript --vanilla ${CODE_FOLDER}check_covariates.R             > ${OUTPUT_FOLDER}check_cov_log.out 2> ${OUTPUT_FOLDER}check_cov_log.err
Rscript --vanilla ${CODE_FOLDER}check_covariate_correlations.R > ${OUTPUT_FOLDER}check_cor_log.out 2> ${OUTPUT_FOLDER}check_cor_log.err

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
