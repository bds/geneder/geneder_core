#!/bin/bash -l
#SBATCH -J geneder:04:match
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 4
#SBATCH --time=0-0:25:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
GEO_PLATFORM_FOLDER=/home/users/ltranchevent/Data/GeneDER/Original/Platforms/
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/04/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/04-Prepare_datasets/

# Loading modules.
resif-load-swset-legacy
module load lang/R/3.6.2-foss-2019b-bare

# Load configuration
source ../libs/conf/confSH.sh
create_variables ../Confs/datasets_config.yml
create_variables ../Confs/platforms_config.yml

# Get the biomart data.
nbPlatforms=${#platforms__platform_name[@]}
for (( i=0; i<$nbPlatforms; i++ ))
do
	platformName=${platforms__platform_name[$i]}
	platformBiomartName=${platforms__biomart_name[$i]}
	platformGEOName=${platforms__geo_name[$i]}
	if [ "${platformBiomartName}" != "NA" ]
	then
		# Get the official gene names.
		wget -O ${OUTPUT_FOLDER}${platformName}_genenames_raw.tsv 'http://www.ensembl.org/biomart/martservice?query=<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE Query><Query  virtualSchemaName = "default" formatter = "TSV" header = "0" uniqueRows = "1" count = "" datasetConfigVersion = "0.6"><Dataset name = "hsapiens_gene_ensembl" interface = "default"><Filter name = "biotype" value = "protein_coding"/><Attribute name = "'${platformBiomartName}'"/><Attribute name = "external_gene_name"/></Dataset></Query>'
		awk 'BEGIN{FS=OFS="\t"}{if ($1 != "") {print $0}}' ${OUTPUT_FOLDER}${platformName}_genenames_raw.tsv | awk '{if (t[$1]) {t[$1]=t[$1]"|"$2} else {t[$1]=$2}} END{for (i in t) {if (i != "") {print i"\t"t[i]}}}' | sort -u > ${OUTPUT_FOLDER}${platformName}_genenames.tsv
		rm ${OUTPUT_FOLDER}${platformName}_genenames_raw.tsv
		sleep 2s
	else
		if [ "${platformGEOName}" != "NA" ]
		then
			# We use the GEO data
			cut -f -2 ${GEO_PLATFORM_FOLDER}${platformGEOName}_gene_official.tsv | grep -v OFFICIAL | sort -u > ${OUTPUT_FOLDER}${platformName}_genenames.tsv
		else
			# We use manually curated data.
			cut -f -2 ${GEO_PLATFORM_FOLDER}${platformName}_gene_official.tsv | grep -v OFFICIAL | sort -u > ${OUTPUT_FOLDER}${platformName}_genenames.tsv
		fi
	fi
done

# Refining the biomart matchings.
Rscript --vanilla ${CODE_FOLDER}create_probelists.R > ${OUTPUT_FOLDER}create_ps_log.out  2> ${OUTPUT_FOLDER}create_ps_log.err
Rscript --vanilla ${CODE_FOLDER}match_probes.R      > ${OUTPUT_FOLDER}match_log.out      2> ${OUTPUT_FOLDER}match_log.err

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}

