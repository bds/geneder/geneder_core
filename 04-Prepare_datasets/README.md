# Objectives
The first objectives of this step is clean the datasets, i.e., remove the arrays that have been flagged during the previous steps due to various errors (referred to as QC-I, PROC-no-converg, QC-II-SCAN, QC-II-GCRMA, CLIN-no-age or CLIN-no-gender). The second objective is to define which probes are going to be used to represent which genes for each platform (removing complex or ambiguous matches).

# Details and instructions
The datasets are processed one by one to remove the bad arrays and update the clinical files accordingly. The clinical data is also updated to include gender predictions. The necessary information is described in the local configuration file, that needs to be manually updated based on the outputs of the previous steps. Please note that the 'Batch.tsv' file is also updated since it might be used after preprocessing (for limma).
```
make clean_outputs
make data
```

The age and category balances are then checked for all datasets. No statistics is derived but plots are created for manual inspection.
```
make check
```

The most appropriate gene-probe match is defined (per dataset/platform individually).
```
make match
```

For manual inspection, a document that contains all figures is then generated.
```
make doc
```

# Prerequisites
The prerequisites are to have the preprocessed data for all datasets (Step 02) as well as the predicted gender information (Step 03).