#!/bin/bash -l
#SBATCH -J geneder:04:doc_pvca
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --time=0-00:01:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# I/Os and parameters
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/04/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/04-Prepare_datasets/

# Load configuration
source ../libs/conf/confSH.sh
create_variables ../Confs/datasets_config.yml
create_variables ../Confs/biomarkers_config.yml

# Clean start
rm -rf ${OUTPUT_FOLDER}results_pvca_summary.*

# Print header
echo '\documentclass[]{article}' > ${OUTPUT_FOLDER}results_pvca_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
echo '\usepackage{graphicx}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
echo '\title{GeneDER - step 04 - Prepare datasets - PVCA analysis}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
echo '\author{Leon-Charles Tranchevent}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
echo '\begin{document}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
echo '\maketitle' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
echo '\textsl{}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
echo '\begin{abstract}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
echo 'This document summarizes the results of the step 04-Prepare\_datasets - PVCA analysis.' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
echo 'Weighted average proportion variance for various combination of covariates are plotted.\\' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
echo 'Note: this document is automatically generated.' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
echo '\end{abstract}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
echo '\clearpage' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_pvca_summary.tex

# Per dataset analysis.
nbDatasets=${#datasets__dataset_name[@]}
for (( i=0; i<$nbDatasets; i++ ))
do
	datasetName=${datasets__dataset_name[$i]}
	datasetHasAge=${datasets__has_age[$i]}

	# Boxplots of Age / Batch / PMI / RIN against the 4 patient groups of interest.
	echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\centering' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	if [ -f  ${OUTPUT_FOLDER}/${datasetName}_Age_balance.png ]
		then
			echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_Age_balance.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	fi
	if [ -f  ${OUTPUT_FOLDER}/${datasetName}_Batch_balance.png ]
		then
			echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_Batch_balance.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	fi
	if [ -f  ${OUTPUT_FOLDER}/${datasetName}_PMI_balance.png ]
		then
			echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_PMI_balance.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	fi
	if [ -f  ${OUTPUT_FOLDER}/${datasetName}_RIN_balance.png ]
		then
			echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_RIN_balance.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	fi
	echo '	\caption{Covariate distributions for the four patient groups of interest for dataset '"${datasetName}"'.' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '\end{figure}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '' >> ${OUTPUT_FOLDER}results_pvca_summary.tex

	# Pheno correlations lolliplots.
	echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\centering' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_pheno_covariate_ds_correlation_pearson.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_pheno_covariate_ds_correlation_kruskal.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_pheno_covariate_gdrds_correlation_pearson.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_pheno_covariate_gdrds_correlation_kruskal.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\caption{Correlation P values for dataset '"${datasetName}"' (VSN).' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	Correlation is computed between the main PCs from PCA (capturing at least 75\% of the variance) and' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	the clinical covariates.}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '\end{figure}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '' >> ${OUTPUT_FOLDER}results_pvca_summary.tex

	echo '\clearpage' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '' >> ${OUTPUT_FOLDER}results_pvca_summary.tex

	# PVCA of all covariates (VSN).
	echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\centering' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_VSN_pvca_main.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_VSN_pvca_all.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	if [ -f  ${OUTPUT_FOLDER}/${datasetName}_VSN_pvca_main_plus_Age.png ]
		then
			echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_VSN_pvca_main_plus_Age.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	fi
	if [ -f  ${OUTPUT_FOLDER}/${datasetName}_VSN_pvca_main_plus_Batch.png ]
		then
			echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_VSN_pvca_main_plus_Batch.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	fi
	if [ -f  ${OUTPUT_FOLDER}/${datasetName}_VSN_pvca_main_plus_PMI.png ]
		then
			echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_VSN_pvca_main_plus_PMI.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	fi
	if [ -f  ${OUTPUT_FOLDER}/${datasetName}_VSN_pvca_main_plus_RIN.png ]
		then
			echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_VSN_pvca_main_plus_RIN.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	fi
	echo '	\caption{PVCA plots for dataset '"${datasetName}"' (VSN). Each' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	plot is created with a specific combination of covariates.' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '\end{figure}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '' >> ${OUTPUT_FOLDER}results_pvca_summary.tex

	# PC correlations lolliplots (VSN).
	echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\centering' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_VSN_pc_covariate_correlation_pearson.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_VSN_pc_covariate_correlation_kruskal.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\caption{Correlation P values for dataset '"${datasetName}"' (VSN).' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	Correlation is computed between the main PCs from PCA (capturing at least 75\% of the variance) and' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	the clinical covariates.}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '\end{figure}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '' >> ${OUTPUT_FOLDER}results_pvca_summary.tex

	echo '\clearpage' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '' >> ${OUTPUT_FOLDER}results_pvca_summary.tex

	# PVCA of all covariates (no VSN).
	echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\centering' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_noVSN_pvca_main.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_noVSN_pvca_all.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	if [ -f  ${OUTPUT_FOLDER}/${datasetName}_noVSN_pvca_main_plus_Age.png ]
		then
			echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_noVSN_pvca_main_plus_Age.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	fi
	if [ -f  ${OUTPUT_FOLDER}/${datasetName}_noVSN_pvca_main_plus_Batch.png ]
		then
			echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_noVSN_pvca_main_plus_Batch.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	fi
	if [ -f  ${OUTPUT_FOLDER}/${datasetName}_noVSN_pvca_main_plus_PMI.png ]
		then
			echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_noVSN_pvca_main_plus_PMI.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	fi
	if [ -f  ${OUTPUT_FOLDER}/${datasetName}_noVSN_pvca_main_plus_RIN.png ]
		then
			echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_noVSN_pvca_main_plus_RIN.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	fi
	echo '	\caption{PVCA plots for dataset '"${datasetName}"' (no VSN). Each' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	plot is created with a specific combination of covariates.' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '\end{figure}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '' >> ${OUTPUT_FOLDER}results_pvca_summary.tex

	# PC correlations lolliplots (no VSN).
	echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\centering' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_noVSN_covariate_correlation_pearson.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\includegraphics[scale=0.35]{'"$OUTPUT_FOLDER"''"${datasetName}"'_noVSN_covariate_correlation_kruskal.png}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	\caption{Correlation P values for dataset '"${datasetName}"' (no VSN).' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	Correlation is computed between the main PCs from PCA (capturing at least 75\% of the variance) and' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '	the clinical covariates.}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '\end{figure}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '' >> ${OUTPUT_FOLDER}results_pvca_summary.tex

	echo '\clearpage' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
	echo '' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
done

# Print footer
echo '' >> ${OUTPUT_FOLDER}results_pvca_summary.tex
echo '\end{document}' >> ${OUTPUT_FOLDER}results_pvca_summary.tex

# Compilation
pdflatex -synctex=1 -interaction=nonstopmode ${OUTPUT_FOLDER}results_pvca_summary.tex
mv results_pvca_summary.pdf ${OUTPUT_FOLDER}
rm results_pvca_summary*

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
