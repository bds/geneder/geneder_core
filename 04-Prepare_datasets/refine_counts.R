#!/usr/bin/env Rscript

# ================================================================================================
# Libraries
# ================================================================================================
library("yaml")
library("tidyverse")
library("reshape2")
source("../libs/conf/confR.R")
source("../libs/utils/utils.R")
message(paste0("[", Sys.time(), "] Libraries loaded."))

# ================================================================================================
# Configuration
# ================================================================================================
options(bitmapType = "cairo")
config          <- read_config(config_dirs = c("../Confs/", "./"))
output_data_dir <- paste0(config$global_data_dir, config$local_data_dir)
input_data_dir  <- paste0(config$global_data_dir, config$local_input_data_dir)
message(paste0("[", Sys.time(), "] Configuration done."))

# ================================================================================================
# Main
# ================================================================================================

# We read the raw counts performed in bash.
nb_samples_fn  <- paste0(output_data_dir, "clinical_categories_summarized.tsv")
nb_samples_raw <- read.table(nb_samples_fn,
                             header    = FALSE,
                             sep       = "\t",
                             row.names = NULL,
                             as.is     = TRUE)
names(nb_samples_raw) <- c("Dataset", "Cond", "Nb")

# We go from the long to the wide format.
nb_samples <- dcast(nb_samples_raw, Dataset ~ Cond, value.var = "Nb")
nb_samples[is.na(nb_samples)] <- 0

if (is.null(nb_samples$F.Control)) {
  nb_samples$F.Control <- rep(0, dim(nb_samples)[1])
}
if (is.null(nb_samples$F.PD)) {
  nb_samples$F.PD      <- rep(0, dim(nb_samples)[1])
}
if (is.null(nb_samples$M.Control)) {
  nb_samples$M.Control <- rep(0, dim(nb_samples)[1])
}
if (is.null(nb_samples$M.PD)) {
  nb_samples$M.PD      <- rep(0, dim(nb_samples)[1])
}

# We add more columns based on the 4 initial categories.
# We first sum to get the total counts for more generic categories (i.e., F = F.PD + F.Control).
# We then also compute the minimum of two or more categories (to assess whether we can run a
# differential analysis or whether there are not enough samples).
nb_samples <- nb_samples %>%
  dplyr::mutate(F = F.PD + F.Control, M = M.PD + M.Control, PD = M.PD + F.PD,
                Control = M.Control + F.Control, All = PD + Control) %>%
  dplyr::rowwise() %>%
  dplyr::mutate(Gender = min(F, M), Disease_status = min(PD, Control)) %>%
  dplyr::mutate(Disease_status_females = min(F.PD, F.Control),
                Disease_status_males   = min(M.PD, M.Control),
                Gender_PD              = min(M.PD, F.PD),
                Gender_Control         = min(M.Control, F.Control),
                Gender_disease_status  = min(F.PD, F.Control, M.PD, M.Control))

# We finalize the data-frame with row names and no missing values.
nb_samples[is.na(nb_samples)] <- 0
nb_samples <- data.frame(nb_samples)
row.names(nb_samples) <- str_replace_all(nb_samples$Dataset, "-", ".")

# We save it again (to the same file).
write.table(nb_samples,
            file      = nb_samples_fn,
            sep       = "\t",
            quote     = FALSE)
remove(nb_samples_raw, nb_samples, nb_samples_fn)

# We log the session details for reproducibility.
rm(config, input_data_dir, output_data_dir)
sessionInfo()
