#!/bin/bash -l
#SBATCH -J geneder:02:prepro
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 20
#SBATCH --time=0-10:00:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/02/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/02-Preprocessing/

# Loading modules.
resif-load-swset-legacy
module load lang/R/3.6.2-foss-2019b-bare

# Load configuration
source ../libs/conf/confSH.sh
create_variables ../Confs/datasets_config.yml

# Actual jobs

# I- CGRMA via Affymetrix Power Tools (tag: APT-GCRMA)
/bin/bash ${CODE_FOLDER}run_apt_gcrma.sh >> ${OUTPUT_FOLDER}apt_log.out 2>> ${OUTPUT_FOLDER}apt_log.err

# II- General workflow with all processing methods.
nbDatasets=${#datasets__dataset_name[@]}
for (( i=0; i<$nbDatasets; i++ ))
do
	datasetName=${datasets__dataset_name[$i]}
	echo "== Job $i started (${datasetName}) =="
    if [ -d "${OUTPUT_FOLDER}${datasetName}" ]; then
	  echo "== Data already there - job not performed ($i, ${datasetName}) =="
    else
  	  mkdir ${OUTPUT_FOLDER}${datasetName}/
	  Rscript --vanilla ${CODE_FOLDER}preprocess.R ${datasetName} >> ${OUTPUT_FOLDER}${datasetName}/preprocessing_log.out 2>> ${OUTPUT_FOLDER}${datasetName}/preprocessing_log.err
	fi
    echo "== Job $i ended (${datasetName}) =="
done

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
