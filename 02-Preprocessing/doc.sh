#!/bin/bash -l
#SBATCH -J geneder:02:doc
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --time=0-00:02:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# I/Os and parameters
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/02/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/02-Preprocessing/

# Load configuration
source ../libs/conf/confSH.sh
create_variables ../Confs/datasets_config.yml
create_variables ../Confs/project_config.yml

# Clean start
rm -rf ${OUTPUT_FOLDER}results_summary.*

# Print header
echo '\documentclass[]{article}' > ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\usepackage{graphicx}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\title{GeneDER - step 02 - Pre-processing}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\author{Leon-Charles Tranchevent}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\begin{document}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\maketitle' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\textsl{}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\begin{abstract}' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'This document summarizes the results of the step 02-Preprocessing, in which raw data are pre-processed.' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'In particular, this document focuses on the necessity (or not) to stabilize the variance (using vsn). \\' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'Note: this document is automatically generated.' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\end{abstract}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex

# Mean vs SD plots for all datasets.
nbDatasets=${#datasets__dataset_name[@]}
for (( i=0; i<$nbDatasets; i++ ))
do
	datasetName=${datasets__dataset_name[$i]}

	# If possible, we display the results on the raw data.
	if [ -f  ${OUTPUT_FOLDER}${datasetName}/${datasetName}_raw_meansd_ranks.png ]
	then

		# Only mean vs SD plots here.
		echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\centering' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\includegraphics[scale=0.28]{'"$OUTPUT_FOLDER"''"${datasetName}"'/'"${datasetName}"'_raw_meansd_ranks.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\includegraphics[scale=0.28]{'"$OUTPUT_FOLDER"''"${datasetName}"'/'"${datasetName}"'_raw_meansd_vals.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\includegraphics[scale=0.28]{'"$OUTPUT_FOLDER"''"${datasetName}"'/'"${datasetName}"'_raw_meansd_ranks_vsn.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\includegraphics[scale=0.28]{'"$OUTPUT_FOLDER"''"${datasetName}"'/'"${datasetName}"'_raw_meansd_vals_vsn.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\caption{Mean vs sd plots for the '"$datasetName"' dataset (on raw data). (Top) Before applying variance stabilization.' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	(Bottom) After applying variance stabilization. (Left) Ranked based plots. (Right) Intensity based plots.}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex
	fi

	# For each normalization method.
	nbNorms=${#normalizations__name[@]}
	for (( j=0; j<$nbNorms; j++ ))
	do
		normName=${normalizations__name[$j]}

		# By default, first no batch correction.
		# Start with the mean vs SD plots.
		echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\centering' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\includegraphics[scale=0.28]{'"$OUTPUT_FOLDER"''"${datasetName}"'/'"${normName}"'/'"${datasetName}"'_normalized_'"${normName}"'_nobatchcorrection_meansd_ranks.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\includegraphics[scale=0.28]{'"$OUTPUT_FOLDER"''"${datasetName}"'/'"${normName}"'/'"${datasetName}"'_normalized_'"${normName}"'_nobatchcorrection_meansd_vals.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\includegraphics[scale=0.28]{'"$OUTPUT_FOLDER"''"${datasetName}"'/'"${normName}"'/'"${datasetName}"'_normalized_'"${normName}"'_nobatchcorrection_meansd_ranks_vsn.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\includegraphics[scale=0.28]{'"$OUTPUT_FOLDER"''"${datasetName}"'/'"${normName}"'/'"${datasetName}"'_normalized_'"${normName}"'_nobatchcorrection_meansd_vals_vsn.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\caption{Mean vs sd plots for the '"$datasetName"' dataset ('"${normName}"', no batch correction). (Top) Before applying variance stabilization.' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	(Bottom) After applying variance stabilization. (Left) Ranked based plots. (Right) Intensity based plots.}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex

		# Then the scatterplot.
		echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\centering' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\includegraphics[scale=0.25]{'"$OUTPUT_FOLDER"''"${datasetName}"'/'"${normName}"'/'"${datasetName}"'_normalized_'"${normName}"'_nobatchcorrection_data_vs_vsn.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\caption{Scatter plot between the original data (pre-processed) and the same data after variance stabilization for' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	the '"$datasetName"' dataset ('"${normName}"', no batch correction).}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex
#				echo '\clearpage' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex

		# If possible, second with batch correction.
		if [ -f  ${OUTPUT_FOLDER}${datasetName}/${normName}/${datasetName}_normalized_${normName}_batchcorrection_meansd_ranks.png ]
		then
			# Start with the mean vs SD plots.
			echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '	\centering' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '	\includegraphics[scale=0.28]{'"$OUTPUT_FOLDER"''"${datasetName}"'/'"${normName}"'/'"${datasetName}"'_normalized_'"${normName}"'_batchcorrection_meansd_ranks.png}' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '	\includegraphics[scale=0.28]{'"$OUTPUT_FOLDER"''"${datasetName}"'/'"${normName}"'/'"${datasetName}"'_normalized_'"${normName}"'_batchcorrection_meansd_vals.png}' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '	\includegraphics[scale=0.28]{'"$OUTPUT_FOLDER"''"${datasetName}"'/'"${normName}"'/'"${datasetName}"'_normalized_'"${normName}"'_batchcorrection_meansd_ranks_vsn.png}' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '	\includegraphics[scale=0.28]{'"$OUTPUT_FOLDER"''"${datasetName}"'/'"${normName}"'/'"${datasetName}"'_normalized_'"${normName}"'_batchcorrection_meansd_vals_vsn.png}' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '	\caption{Mean vs sd plots for the '"$datasetName"' dataset ('"${normName}"', batch correction). (Top) Before applying variance stabilization.' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '	(Bottom) After applying variance stabilization. (Left) Ranked based plots. (Right) Intensity based plots.}' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '' >> ${OUTPUT_FOLDER}results_summary.tex

			# Then the scatterplot.
			echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '	\centering' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '	\includegraphics[scale=0.25]{'"$OUTPUT_FOLDER"''"${datasetName}"'/'"${normName}"'/'"${datasetName}"'_normalized_'"${normName}"'_batchcorrection_data_vs_vsn.png}' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '	\caption{Scatter plot between the original data (pre-processed) and the same data after variance stabilization for' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '	the '"$datasetName"' dataset ('"${normName}"', batch correction).}' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '\clearpage' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '' >> ${OUTPUT_FOLDER}results_summary.tex
		fi
	done
done

# Print footer
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\end{document}' >> ${OUTPUT_FOLDER}results_summary.tex

# Compilation
pdflatex -synctex=1 -interaction=nonstopmode ${OUTPUT_FOLDER}results_summary.tex
mv results_summary.pdf ${OUTPUT_FOLDER}
rm results_summary*

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
