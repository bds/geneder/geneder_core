# Objectives
The objectives of this step are to preprocess the raw data and to save them for further use.

# Details and instructions
All Affymetrix datasets are preprocessed using SCAN and GC-RMA. Illumina and Agilent datasets are preprocessed using dedicated R libraries (limma and beadarrays). The RNA-seq data are just post-processed since the preprocessing took place before. The Makefile contains the commands to launch all jobs.

The data are stored as TSV files.
```
make clean_outputs
make preprocess
```

For the datasets analyzed through SCAN, we can then make a plot of the preprocessing results (to identify the arrays for which SCAN modeling might have failed). We don't do any post-processing of the normalization procedure for Illumina or Agilent arrays (QC are run anyway in all cases).
```
make get_log
```

We can then investigate the effect of applying a variance stabilization method on the data (*e.g*, to control heteroscedasticity)
```
make vsn
```

Finally, we create a report about the processing and the variance stabilization methods so that we can manually check which normalization procedure makes more sense or whether it is indeed necessary to correct for variance bias.
```
make doc
```

# Prerequisites
In general, the only prerequisite is to have the raw data (mostly from GEO) in the Data folder. For the array-based datasets, there should be one folder per dataset, with a '/RAW/' folder with the raw data as CEL files. For the RNA-seq dataset, the pre-processed data should be available as TSV files containing read counts. For SCAN in particular, it is not necessary to run the quality control before running the preprocessing since arrays are preprocessed independently (unless one has many problematic arrays, which would take CPU time for nothing). For all the other methods however, this is not the case, and the bad quality arrays needs to be filtered before pre-processing.
