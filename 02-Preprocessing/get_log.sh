#!/bin/bash -l
#SBATCH -J geneder:02:log
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --time=0-00:02:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/02/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/02-Preprocessing/

# Loading modules.
resif-load-swset-legacy
module load lang/R/3.6.2-foss-2019b-bare

# Load configuration
source ../libs/conf/confSH.sh
create_variables ../Confs/datasets_config.yml

# Function that extract the interesting bits from the logs.
function extract_data_from_log() {

	# We first start by getting the array names.
	grep -i 'Reading' ${OUTPUT_FOLDER}$1/preprocessing_log.out | cut -f 10 -d '/' | sed -r 's/^/'"$1"'\t/g' | grep -i cel > ${OUTPUT_FOLDER}$1/preprocessing_log_short0.tsv

	# Then, we get the final model (Number of iterations and proportion of background probes).
	grep -i 'iterations' ${OUTPUT_FOLDER}$1/preprocessing_log.err | cut -f 3,9 -d ' ' | sed -r 's/limit...//g' | sed -r 's/ /\t/g' > ${OUTPUT_FOLDER}$1/preprocessing_log_short1.tsv

	# We then obtain the convergence value of the last iteration (c).
	grep -B 1 -i 'iterations' ${OUTPUT_FOLDER}$1/preprocessing_log.err | grep Attempting | cut -f 4,7 -d ' ' | sed -r 's/,//g' | sed -r 's/ /\t/g' > ${OUTPUT_FOLDER}$1/preprocessing_log_short2.tsv

	# Last, we combine all together in one file per dataset and clean up temporaty files.
	paste ${OUTPUT_FOLDER}$1/preprocessing_log_short*.tsv > ${OUTPUT_FOLDER}$1/scanlog.tsv
	rm -rf ${OUTPUT_FOLDER}$1/preprocessing_log_short*.tsv
}

# We extract data for all datasets.
for i in "${datasets__dataset_name[@]}"
do
	if [ "$i" != "" ]
	then
		extract_data_from_log $i
	fi
done

# We concatenate across datasets.
echo 'dataset	sample	nb_iterations	prop_back	last_iteration	convergence' > ${OUTPUT_FOLDER}SCANlog_combined.tsv
cat ${OUTPUT_FOLDER}*/scanlog.tsv >> ${OUTPUT_FOLDER}SCANlog_combined.tsv

# We create a plot based on the collected log.
Rscript --vanilla ${CODE_FOLDER}plot_scan_log.R > ${OUTPUT_FOLDER}plot_log.out 2> ${OUTPUT_FOLDER}plot_log.err

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
