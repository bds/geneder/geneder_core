#!/usr/bin/env Rscript

# ================================================================================================
# Libraries
# ================================================================================================
library("yaml")
library("ArrayUtils")
library("Biobase")
library("vsn")
library("affy")
library("preprocessCore")
source("../libs/conf/confR.R")
message(paste0("[", Sys.time(), "] Libraries loaded."))

# ================================================================================================
# Configuration
# ================================================================================================
options(bitmapType = "cairo")
config          <- read_config(config_dirs = c("../Confs/", "./"))
raw_data_dir    <- config$global_raw_data_dir
output_data_dir <- paste0(config$global_data_dir, config$local_data_dir)

# Optional command line parameter to process only one dataset.
args <- commandArgs(trailingOnly = TRUE)
selected_dataset_name <- ""
if (length(args) > 0) {
  selected_dataset_name <- args[1]
}
rm(args)
message(paste0("[", Sys.time(), "] Configuration done."))

# ================================================================================================
# Functions
# ================================================================================================

#' @title Run the VSN method for a given configuration.
#'
#' @description This methods takes a given configuration and run VSN on it to try to stablize the
#' variance. It creates several plots before and after VSN is applied and also save the entire
#' expression matrix after the correction.
#'
#' @param output_data_subdir The folder in chich the expression data is stored. It will also be
#' used to store the figures and the corrected data.
#' @param dataset_name The name of the dataset to consider.
#' @param normalization_name The name of the normalization with which the dataset was
#' pre-processed.
#' @param batchcorrectin_tag The batch correction tag used to the name the files.
run_vsn <- function(output_data_subdir, dataset_name, normalization_name, batchcorrection_tag) {
  # We read the expression data.
  input_file_name <- paste0(output_data_subdir, dataset_name, "_normalized_",
                            normalization_name, "_", batchcorrection_tag, ".tsv")

  exp_data        <- as.matrix(read.delim(input_file_name, row.names = 1))
  exp_data_min    <- min(min(exp_data))
  exp_data_max    <- max(max(exp_data))
  rm(input_file_name)

  # We check if there is one value that is over-represented.
  exp_data_counts  <- table(unlist(exp_data))
  exp_data_majperc <- 100 * max(exp_data_counts) / sum(exp_data_counts)
  rm(exp_data_counts)

  # We plot the mean vs sdev before variance stabilization.
  output_file_name <- paste0(output_data_subdir, dataset_name, "_normalized_",
                             normalization_name, "_", batchcorrection_tag, "_meansd_ranks.png")
  png(output_file_name)
  vsn::meanSdPlot(exp_data)
  dev.off()
  output_file_name <- paste0(output_data_subdir, dataset_name, "_normalized_",
                             normalization_name, "_", batchcorrection_tag, "_meansd_vals.png")
  png(output_file_name)
  vsn::meanSdPlot(exp_data, ranks = FALSE)
  dev.off()
  rm(output_file_name)

  # If necessary, we had little noise to the data, otherwise vsn might fail.
  if (exp_data_majperc > 5) {
    noise      <- rnorm(dim(exp_data)[1] * dim(exp_data)[2], mean = 0, sd = 0.001)
    dim(noise) <- c(dim(exp_data)[1], dim(exp_data)[2])
    exp_data   <- exp_data + noise
    rm(noise)
  }
  rm(exp_data_majperc)

  # We sometimes need to make several attempts since the GC-RMA data can lead to errors
  # because they might contain many ties (usage of quantile).
  exp_data_vsn <- NULL
  for (k in 1:5) {
    exp_data_vsn <- tryCatch({
      # We use vsn to stabilize the variance.
      vsn::vsn2(exp_data, calib = "none")
    }, warning = function(w) { #nolintr
      return(NULL)
    }, error = function(e) { #nolintr
      message(paste0("[", Sys.time(), "] [", dataset_name, "][", normalization_method,
                     "] VSN attempt ", k, " failed."))
      return(NULL)
    }, finally = {}) #nolintr

    # If we managed to get vsn to work, we break of the loop,
    # otherwise we continue to add noise...
    if (!is.null(exp_data_vsn)) {
      break
    } else {
      noise      <- rnorm(dim(exp_data)[1] * dim(exp_data)[2], mean = 0, sd = 0.001)
      dim(noise) <- c(dim(exp_data)[1], dim(exp_data)[2])
      exp_data   <- exp_data + noise
    }
  } # End of loop on k of 5 iterations.

  # We plot the mean vs sdev after variance stabilization.
  output_file_name <- paste0(output_data_subdir, dataset_name, "_normalized_",
                             normalization_name, "_", batchcorrection_tag, "_meansd_ranks_vsn.png")
  png(output_file_name)
  vsn::meanSdPlot(exp_data_vsn)
  dev.off()
  output_file_name <- paste0(output_data_subdir, dataset_name, "_normalized_",
                             normalization_name, "_", batchcorrection_tag, "_meansd_vals_vsn.png")
  png(output_file_name)
  vsn::meanSdPlot(exp_data_vsn, ranks = FALSE)
  dev.off()
  rm(output_file_name)

  # We rescale the data as to have it on the same limits than the original data.
  scaling_factor   <- (max(exp_data_vsn@hx) - min(exp_data_vsn@hx))
  scaling_factor   <- scaling_factor / (exp_data_max - exp_data_min)
  exp_data_vsn_scl <- (exp_data_vsn@hx / scaling_factor)
  exp_data_vsn_scl <- exp_data_vsn_scl + exp_data_min - min(exp_data_vsn@hx / scaling_factor)

  # Scatter plot of the processed data versus the scaled vsn data.
  output_file_name <- paste0(output_data_subdir, dataset_name, "_normalized_",
                             normalization_name, "_", batchcorrection_tag, "_data_vs_vsn.png")
  png(output_file_name)
  plot(exp_data, exp_data_vsn_scl)
  dev.off()
  rm(output_file_name)

  # We save the data.
  output_file_name <- paste0(output_data_subdir, dataset_name, "_normalized_",
                             normalization_name, "_", batchcorrection_tag, "_vsn.tsv")
  utils::write.table(exp_data_vsn_scl, file = output_file_name, sep = "\t", quote = FALSE)
  rm(scaling_factor, exp_data_min, exp_data_max)
  rm(exp_data_vsn, exp_data_vsn_scl, output_file_name)
} # End function run_vsn.

# ================================================================================================
# Main
# ================================================================================================

# We do all datasets one by one.
for (i in seq_len(length(config$datasets))) {

  # We get the dataset details.
  dataset      <- config$datasets[[i]]
  dataset_name <- dataset$dataset_name
  raw_data_subdir    <- paste0(raw_data_dir, dataset_name, "/")

  # We preprocess the current dataset (if necessary).
  if (selected_dataset_name == "" || selected_dataset_name == dataset_name) {

    for (j in seq_len(length(config$normalizations))) {

      # We define the current normalization procedure.
      normalization        <- config$normalizations[[j]]
      normalization_method <- normalization$methods[normalization$platforms == dataset$platform]
      message(paste0("[", Sys.time(), "] [", dataset_name, "][", normalization_method,
                     "] Started..."))

      # We define the I/Os.
      output_data_subdir <- paste0(output_data_dir, dataset_name, "/", normalization$name, "/")

      # We may have batches and therefore two data matrices.
      if (dataset$has_batches) {
        # There are batches, we start with processing the batch corrected data.
        message(paste0("[", Sys.time(), "] [", dataset_name, "][", normalization_method,
                       "] Starting variance stabilizing (with batches)."))
        run_vsn(output_data_subdir, dataset_name, normalization$name, "batchcorrection")
        message(paste0("[", Sys.time(), "] [", dataset_name, "][", normalization_method,
                       "] Variance stabilizing (with batches) finished."))

        # We continue with the data without batch correction.
        message(paste0("[", Sys.time(), "] [", dataset_name, "][", normalization_method,
                       "] Starting variance stabilizing (without batch effect correction)."))
        run_vsn(output_data_subdir, dataset_name, normalization$name, "nobatchcorrection")
        message(paste0("[", Sys.time(), "] [", dataset_name, "][", normalization_method,
                       "] Variance stabilizing (without batch effect correction) finished."))
      } else {
        # We have no batch, so only one run.
        message(paste0("[", Sys.time(), "] [", dataset_name, "][", normalization_method,
                       "] Starting variance stabilizing (without batch)."))
        run_vsn(output_data_subdir, dataset_name, normalization$name, "nobatchcorrection")
        message(paste0("[", Sys.time(), "] [", dataset_name, "][", normalization_method,
                       "] Variance stabilizing (without batch) finished."))
      } # End if dataset has known batches.
      message(paste0("[", Sys.time(), "] [", dataset_name, "][", normalization_method,
                     "] Ended."))
      rm(raw_data_subdir, output_data_subdir, normalization, normalization_method)
    } # End for each normalization.
  } # End if dataset should be analysed.
  rm(dataset, dataset_name, dataset_groups)
} # End for each dataset.

# We log the session details for reproducibility.
rm(i, selected_dataset_name, raw_data_dir, output_data_dir, config)
sessionInfo()
