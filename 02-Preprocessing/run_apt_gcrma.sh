#!/bin/bash -l

# I/Os and parameters
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/02/

# Load configuration
source ../libs/conf/confSH.sh
create_variables ../Confs/datasets_config.yml
create_variables ../Confs/project_config.yml
create_variables ../Confs/platforms_config.yml

# We set up the results folder that will contain all files for all datasets.
rm -rf ${OUTPUT_FOLDER}apt_gcrma
mkdir ${OUTPUT_FOLDER}apt_gcrma

# We run the APT-GCRMA for all datasets (or at least all the Affymetrix ones).
nbDatasets=${#datasets__dataset_name[@]}
for (( i=0; i<$nbDatasets; i++ ))
do
	datasetName=${datasets__dataset_name[$i]}
	platformName=${datasets__platform[$i]}
	if [ "${platformName}" == "Affymetrix" ]
	then
		# We set up the temporary folder for that run.
		rm -rf ${OUTPUT_FOLDER}apt_gcrma_temp
		mkdir ${OUTPUT_FOLDER}apt_gcrma_temp

		# We get the correct CDF file based on the dataset / platform configurations.
		datasetArrayType=${datasets__array_type[$i]}
		cdfName="NA"
		mpsName="NA"
		nbPlatforms=${#platforms__platform_name[@]}
		for (( j=0; j<$nbPlatforms; j++ ))
		do
			platformName=${platforms__platform_name[$j]}
			if [ "${platformName}" == "${datasetArrayType}" ]
			then
				cdfName=${platforms__cdf_name[$j]}
				mpsName=${platforms__mps_name[$j]}
			fi
		done

		# We prepare the APT command. We take care of the cases for which we need to
		# summarize the data at the transcript level (exon arrays). Otherwise, we summarize at
		# the probeset level (by default).
		if [ "${mpsName}" == "NA" ]
		then
			apt_cmd="${apt_script} -a ${apt_script_method} -d ${global_raw_data_dir}Platforms/${cdfName} -o ${OUTPUT_FOLDER}apt_gcrma_temp ${global_raw_data_dir}${datasetName}/RAW/*"
		else
			apt_cmd="${apt_script} -a ${apt_script_method} -d ${global_raw_data_dir}Platforms/${cdfName} -m ${global_raw_data_dir}Platforms/${mpsName} -o ${OUTPUT_FOLDER}apt_gcrma_temp ${global_raw_data_dir}${datasetName}/RAW/*"
		fi

		# We run the APT command and we rename / copy the result file to the real apt folder.
		eval "$apt_cmd"
		mv ${OUTPUT_FOLDER}apt_gcrma_temp/gc-correction.scale-intensities.rma-bg.quant-norm.pm-only.med-polish.summary.txt ${OUTPUT_FOLDER}apt_gcrma/${datasetName}.tsv

		# We clean up the temporary folder for that run.
		rm -rf ${OUTPUT_FOLDER}apt_gcrma_temp
	fi
done
