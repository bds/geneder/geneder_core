#!/bin/bash -l
#SBATCH -J geneder:08:getdb
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 2
#SBATCH --time=0-00:06:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
INPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/07/
ALT_INPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/06/
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/08/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/08-RegulatoryNetwork/

# Actual job in bash.
# We download the latest Dodothea release from omnipath. We use the evidence levels A, B and C and disregard the levels D and E.
curl -o ${OUTPUT_FOLDER}Dorothea_regulons_raw.tsv "https://omnipathdb.org/interactions?datasets=tfregulons&tfregulons_levels=A,B,C&genesymbols=1&fields=sources,tfregulons_level"

# We remove the unnecessary fields. So far, we do not filter the TF that are very frequent.
# The most frequent one (with evidence A, B and C) has ~1k targets.
# This can be changed if we also change the evidence levels to include D as well.
head -n 1 ${OUTPUT_FOLDER}Dorothea_regulons_raw.tsv | cut -f 3,4,6,7,12 > ${OUTPUT_FOLDER}Dorothea_regulons_clean.tsv
grep -v "source_genesymbol" ${OUTPUT_FOLDER}Dorothea_regulons_raw.tsv | cut -f 3,4,6,7,12 | sort -u >> ${OUTPUT_FOLDER}Dorothea_regulons_clean.tsv

# We process the files further to extract the full list of gene ids used by DoRothEA.
cat <(cut -f 1 ${OUTPUT_FOLDER}Dorothea_regulons_clean.tsv) <(cut -f 2 ${OUTPUT_FOLDER}Dorothea_regulons_clean.tsv) | grep -v "source_genesymbol" | grep -v "target_genesymbol" | sort -u > ${OUTPUT_FOLDER}Dorothea_geneids.tsv

# We also extract all gene ids used in GeneDER.
cut -f 2 ${ALT_INPUT_FOLDER}Combined_probe_matching.tsv | grep -v "genes" | sort -u > ${OUTPUT_FOLDER}Geneder_geneids.tsv

# We compute the overlap, and more interestingly the genes from DoRothEA that do not match the GeneDER genes.
comm -23 ${OUTPUT_FOLDER}Dorothea_geneids.tsv ${OUTPUT_FOLDER}Geneder_geneids.tsv > ${OUTPUT_FOLDER}Dorothea_geneids_unmapped.tsv
echo "Unmapped vs all gene ids:"
wc -l ${OUTPUT_FOLDER}Dorothea_geneids_unmapped.tsv
wc -l ${OUTPUT_FOLDER}Dorothea_geneids.tsv

# Now, we focus on getting the official gene names.
# We prepare the XML query.
query_template_part1='<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE Query><Query virtualSchemaName = "default" formatter = "TSV" header = "0" uniqueRows = "0" count = "" datasetConfigVersion = "0.6" ><Dataset name = "hsapiens_gene_ensembl" interface = "default" ><Filter name = "external_synonym" value = "'
query_template_part1bis='<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE Query><Query virtualSchemaName = "default" formatter = "TSV" header = "0" uniqueRows = "0" count = "" datasetConfigVersion = "0.6" ><Dataset name = "hsapiens_gene_ensembl" interface = "default" ><Filter name = "external_gene_name" value = "'
query_template_part2='"/><Attribute name = "external_gene_name" /><Attribute name = "external_synonym" /></Dataset></Query>'
query_template_part2bis='"/><Attribute name = "external_gene_name" /></Dataset></Query>'

# We get the list of genes to query Biomart for.
genes=$(cat ${OUTPUT_FOLDER}Dorothea_geneids_unmapped.tsv | tr "\n" ",")

# We query biomart (from aliases to official gene names).
wget -O ${OUTPUT_FOLDER}temp "http://www.ensembl.org/biomart/martservice?query=${query_template_part1}${genes}${query_template_part2}"
sort -u ${OUTPUT_FOLDER}temp | sort -k2,2 > ${OUTPUT_FOLDER}Dorothea_geneids_unmapped_mapped.tsv
rm -rf ${OUTPUT_FOLDER}temp
echo "Potential multi-mapping:"
cut -f 2 ${OUTPUT_FOLDER}Dorothea_geneids_unmapped_mapped.tsv | sort | uniq -c | sort -g | grep -v '\s1\s'

# We query biomart (from official gene names to official gene names - only to check existence).
wget -O ${OUTPUT_FOLDER}temp "http://www.ensembl.org/biomart/martservice?query=${query_template_part1bis}${genes}${query_template_part2bis}"
sort -u ${OUTPUT_FOLDER}temp | sort -k1,1 > ${OUTPUT_FOLDER}Dorothea_geneids_unmapped_missing.tsv
rm -rf ${OUTPUT_FOLDER}temp

# We log a few counts to inform the user.
echo "Unmapped mapped via Biomart / official name missing in GeneDER / all unmapped:"
cut -f 2 ${OUTPUT_FOLDER}Dorothea_geneids_unmapped_mapped.tsv | sort -u | wc -l
wc -l ${OUTPUT_FOLDER}Dorothea_geneids_unmapped_missing.tsv
wc -l ${OUTPUT_FOLDER}Dorothea_geneids_unmapped.tsv

# Build the final clean map (that would still need to be checked manually).
join -1 2 -2 1 ${OUTPUT_FOLDER}Dorothea_geneids_unmapped_mapped.tsv <(comm -23 <(cut -f 2 ${OUTPUT_FOLDER}Dorothea_geneids_unmapped_mapped.tsv | sort | uniq -c | sort -g | grep '\s1\s' | sed -r 's/1 /1\t/g' | cut -f 2) ${OUTPUT_FOLDER}Dorothea_geneids_unmapped_missing.tsv) | sed -r 's/ /\t/g' > ${OUTPUT_FOLDER}Dorothea_geneids_unmapped_mapped_clean.tsv
join -t$'t' -a 1 -1 1 -2 1 ${OUTPUT_FOLDER}Dorothea_geneids.tsv <(sort -k1,1n ${OUTPUT_FOLDER}Dorothea_geneids_unmapped_mapped_clean.tsv) | cut -f 2 > ${OUTPUT_FOLDER}Dorothea_geneids_automatic.tsv

# Warning for user.
echo "*************************************************************************"
echo "* YOU MAY WANT TO MANUALLY REFINE THE MAPPING IN THE FILE" 
echo "* ${OUTPUT_FOLDER}Dorothea_geneids_unmapped_mapped_clean.tsv"
echo "*************************************************************************"

# Part II: we now only focus on the source genes (because for the Uniprot ids matching, we do not match the targets).
# We process the DoRothEA files further to extract the full list of gene ids used by DoRothEA.
cut -f 1 ${OUTPUT_FOLDER}Dorothea_regulons_clean.tsv | grep -v "source_genesymbol" | sort -u > ${OUTPUT_FOLDER}Dorothea_source_geneids.tsv

# We compute the overlap, and more interestingly the genes from DoRothEA that do not match the GeneDER genes.
comm -23 ${OUTPUT_FOLDER}Dorothea_source_geneids.tsv ${OUTPUT_FOLDER}Geneder_geneids.tsv > ${OUTPUT_FOLDER}Dorothea_source_geneids_unmapped.tsv
echo "Unmapped vs all gene ids:"
wc -l ${OUTPUT_FOLDER}Dorothea_source_geneids_unmapped.tsv
wc -l ${OUTPUT_FOLDER}Dorothea_source_geneids.tsv

# We get the list of genes to query Biomart for.
genes=$(cat ${OUTPUT_FOLDER}Dorothea_source_geneids_unmapped.tsv | tr "\n" ",")

if [ -z "$genes" ] 
then
	touch ${OUTPUT_FOLDER}Dorothea_source_geneids_unmapped_mapped_clean.tsv
else
	# We query biomart (from aliases to official gene names).
	wget -O ${OUTPUT_FOLDER}temp "http://www.ensembl.org/biomart/martservice?query=${query_template_part1}${genes}${query_template_part2}"
	sort -u ${OUTPUT_FOLDER}temp | sort -k2,2 > ${OUTPUT_FOLDER}Dorothea_source_geneids_unmapped_mapped.tsv
	rm -rf ${OUTPUT_FOLDER}temp
	echo "Potential multi-mapping:"
	cut -f 2 ${OUTPUT_FOLDER}Dorothea_source_geneids_unmapped_mapped.tsv | sort | uniq -c | sort -g | grep -v '\s1\s'

	# We query biomart (from official gene names to official gene names - only to check existence).
	wget -O ${OUTPUT_FOLDER}temp "http://www.ensembl.org/biomart/martservice?query=${query_template_part1bis}${genes}${query_template_part2bis}"
	sort -u ${OUTPUT_FOLDER}temp | sort -k1,1 > ${OUTPUT_FOLDER}Dorothea_source_geneids_unmapped_missing.tsv
	rm -rf ${OUTPUT_FOLDER}temp

	# We log a few counts to inform the user.
	echo "Unmapped mapped via Biomart / official name missing in GeneDER / all unmapped:"
	cut -f 2 ${OUTPUT_FOLDER}Dorothea_source_geneids_unmapped_mapped.tsv | sort -u | wc -l
	wc -l ${OUTPUT_FOLDER}Dorothea_source_geneids_unmapped_missing.tsv
	wc -l ${OUTPUT_FOLDER}Dorothea_source_geneids_unmapped.tsv

	# Build the final clean map (that would still need to be check manually).
	join -1 2 -2 1 ${OUTPUT_FOLDER}Dorothea_source_geneids_unmapped_mapped.tsv <(comm -23 <(cut -f 2 ${OUTPUT_FOLDER}Dorothea_source_geneids_unmapped_mapped.tsv | sort | uniq -c | sort -g | grep '\s1\s' | sed -r 's/1 /1\t/g' | cut -f 2) ${OUTPUT_FOLDER}Dorothea_source_geneids_unmapped_missing.tsv) | sed -r 's/ /\t/g' > ${OUTPUT_FOLDER}Dorothea_source_geneids_unmapped_mapped_clean.tsv
fi
join -t$'t' -a 1 -1 1 -2 1 ${OUTPUT_FOLDER}Dorothea_source_geneids.tsv <(sort -k1,1n ${OUTPUT_FOLDER}Dorothea_source_geneids_unmapped_mapped_clean.tsv) | cut -f 2 > ${OUTPUT_FOLDER}Dorothea_source_geneids_automatic.tsv

# Warning for user.
echo "*************************************************************************"
echo "* YOU MAY WANT TO MANUALLY REFINE THE MAPPING IN THE FILE" 
echo "* ${OUTPUT_FOLDER}Dorothea_source_geneids_unmapped_mapped_clean.tsv"
echo "*************************************************************************"

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
