#!/bin/bash -l
#SBATCH -J geneder:08:prep
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 2
#SBATCH --time=0-00:01:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
INPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/07/
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/08/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/08-RegulatoryNetwork/

# Actual job in bash.
# For each comparison of interest, we select the genes based on the P-values.
awk '{if ($4 <= 0.05) print $0}' ${INPUT_FOLDER}SNage_PDVsControl_females_max-avg_all_pivalue_rankings.tsv | sort -k4,4gr | cut -f 1 | sort -u > ${OUTPUT_FOLDER}SNage_PDVsControl_females_max-avg_all_Pvalue_rankings_genes.tsv
awk '{if ($4 <= 0.05) print $0}' ${INPUT_FOLDER}SNage_PDVsControl_males_max-avg_all_pivalue_rankings.tsv   | sort -k4,4gr | cut -f 1 | sort -u > ${OUTPUT_FOLDER}SNage_PDVsControl_males_max-avg_all_Pvalue_rankings_genes.tsv

# We also concatenate all gender relevant files to create the integrated list.
# We do NOT include the PDvscontrol file as this one represents more or less the baseline.
cat ${OUTPUT_FOLDER}SNage_PDVsControl_females_max-avg_all_Pvalue_rankings_genes.tsv ${OUTPUT_FOLDER}SNage_PDVsControl_males_max-avg_all_Pvalue_rankings_genes.tsv | sort -u > ${OUTPUT_FOLDER}SNage_all_max-avg_all_lists_genes.tsv

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
