#!/bin/bash -l
#SBATCH -J geneder:08:carnival
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 10
#SBATCH --time=0-01:00:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Loading modules.
resif-load-swset-legacy
module load lang/R/3.6.2-foss-2019b-bare
module load math/CPLEX/12.9-foss-2019a

# Defining global parameters.
INPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Original/Else/TFs/
GS_INPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/07/
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/08/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/08-RegulatoryNetwork/

# Copy the data locally (the manually refined Funkiapp files).
cp ${INPUT_FOLDER}SelectedTFs_core3.tsv ${OUTPUT_FOLDER}

# Copy the GS / GD files from previous step.
cp ${GS_INPUT_FOLDER}/SNage_PDVsControl_females_max-avg_gd_pivalueref_rankings.tsv ${OUTPUT_FOLDER}
cp ${GS_INPUT_FOLDER}/SNage_PDVsControl_*males_max-avg_gs_pivalue_rankings.tsv     ${OUTPUT_FOLDER}

# Actual job in R.
Rscript --vanilla ${CODE_FOLDER}select_interactions_funkiapp.R > ${OUTPUT_FOLDER}select_ints_funkiapp_log.out 2> ${OUTPUT_FOLDER}select_ints_funkiapp_log.err

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
