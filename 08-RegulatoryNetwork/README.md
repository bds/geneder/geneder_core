# Objectives
The objectives of this step is to investigate the potential regulators behind the observed DEGs. This steps is not fully automated and involved some manual work (like accessing MetaCore GeneGo but also editing files manually and moving these around).
Some of this is not automated on purpose as it is necessary to carrefully review the results of each step before proceding to the next one.

# Details and instructions
We first start by selecting the genes we want to investigate. 
```
make clean_outputs
make prep
```

We prepare the DoRothEA databased by downloading its latest version from OmniPath. We also prepare the gene lists (DoRothEA and the data we have might not use the same symbols for the same genes) in order to increase the coverage. This is done by matching against BioMart (considered as the reference here).
```
make getdb
```
At this stage, it is important to check the Dorothea_geneids_unmapped_mapped_clean.tsv file to make sure that the extra matching are correct. This file contains the gene for which two different ids are used in DoRothEA and in our data.

We create the DoRothEA GRN (simply extracting the regulatory interactions between genes, making sure we match as many genes as possible).

```
make GRNdot
```

GeneGO results are obtained via the dedicated we interface but we refine them here automatically. In particular, we remove genes that were added to the GeneGO GRN for no particular reason. We instead want to focus on genes from our original lists. We also take care of matching the GeneGO gene names to our gene names.
```
make preprefine
make refineGRN1
```
At this stage, it is crucial to manually curate all XXXXX_mapping_refined.tsv files. These files (and the associated XXXXX_mapping_help.txt) will contain the information necessary to resolve the conflicts and missing matching. I use GeneCards and NCBI ENtrezGene to further check the matching. Details are present in the section below.

```
make refineGRN2
```

Last, we refine the networks in order to define the nodes and edges that will be part of the networks.
```
make selectInts
```

# Prerequisites
A prerequisite is to have the results of the functional enrichment (Step 07), as to rely on the same input than GSEA.
