#!/bin/bash -l
#SBATCH -J geneder:08:ggrefine
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 2
#SBATCH --time=0-00:05:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
GG_DATA_FOLDER=/home/users/ltranchevent/Data/GeneDER/Original/Else/GeneGo/
INPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/07/
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/08/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/08-RegulatoryNetwork/

# Actual job in bash.

# Copy the GeneGo data (must have been run manually before).
cp -rf ${GG_DATA_FOLDER}* ${OUTPUT_FOLDER}
cp ${OUTPUT_FOLDER}SNage_all_max-avg_all_lists_genes.tsv ${OUTPUT_FOLDER}SNage_PDVsControl_both_max-avg_all_Pvalue_rankings_genes.tsv

# We need to loop over all mapping files.
rm -rf ${OUTPUT_FOLDER}*_mapping_refined.tsv
rm -rf ${OUTPUT_FOLDER}*_mapping_help.txt
tag="PDVsControl_both"

# We join back the top PI genes and the raw GeneGo mappings to keep only the real GeneDER genes since GeneGo
# is adding some irrelevant stuff from somewhere. This also allows us to have the real inputs ids that are 
# not returned by GeneGo (despite the GUI saying otherwise).
join -t $'\t' -a 1 -1 1 <(sort -t $'\t' -k1,1 ${OUTPUT_FOLDER}SNage_${tag}_max-avg_all_Pvalue_rankings_genes.tsv)  -2 2 <(sort -t $'\t' -k2,2 ${OUTPUT_FOLDER}SNage_${tag}_max-avg_all_Pvalue_rankings_genes_GRN_genego_mapping.tsv | grep -v "Network Object") > ${OUTPUT_FOLDER}SNage_${tag}_max-avg_all_Pvalue_rankings_genes_GRN_genego_mapping_refined.tsv

# We print additional information to help the manual check that should be done after the join to fill in the missing ids.
echo "PI genes not mapped in GeneGo:" >> ${OUTPUT_FOLDER}SNage_${tag}_max-avg_all_Pvalue_rankings_genes_GRN_genego_mapping_help.txt
join -t $'\t' -v 1 -1 1 <(sort -t $'\t' -k1,1 ${OUTPUT_FOLDER}SNage_${tag}_max-avg_all_Pvalue_rankings_genes.tsv) -2 2 <(sort -t $'\t' -k2,2 ${OUTPUT_FOLDER}SNage_${tag}_max-avg_all_Pvalue_rankings_genes_GRN_genego_mapping.tsv | grep -v "Network Object") >> ${OUTPUT_FOLDER}SNage_${tag}_max-avg_all_Pvalue_rankings_genes_GRN_genego_mapping_help.txt
echo "" >>  ${OUTPUT_FOLDER}SNage_${tag}_max-avg_all_Pvalue_rankings_genes_GRN_genego_mapping_help.txt
echo "GeneGo ids not found in Geneder data:" >> ${OUTPUT_FOLDER}SNage_${tag}_max-avg_all_Pvalue_rankings_genes_GRN_genego_mapping_help.txt
join -t $'\t' -v 2 -1 1 <(sort -t $'\t' -k1,1 ${OUTPUT_FOLDER}SNage_${tag}_max-avg_all_Pvalue_rankings_genes.tsv) -2 2 <(sort -t $'\t' -k2,2 ${OUTPUT_FOLDER}SNage_${tag}_max-avg_all_Pvalue_rankings_genes_GRN_genego_mapping.tsv | grep -v "Network Object") >> ${OUTPUT_FOLDER}SNage_${tag}_max-avg_all_Pvalue_rankings_genes_GRN_genego_mapping_help.txt

# The extended_TF is a special case since we do not have relevant top genes based on PI.
# We instead use all genes reported in the rankings (with pi values).
tag="extended_newTFs"

# We join back the geneder genes (all) and the raw GeneGo mappings to keep only the real GeneDER genes since GeneGo
# is adding some irrelevant stuff from somewhere.
join -t $'\t' -1 1 <(cut -f 1  ${INPUT_FOLDER}*rankings.tsv | grep -v Gene | sort -u -k1,1) -2 2 <(sort -t $'\t' -k2,2 ${OUTPUT_FOLDER}genego_${tag}_mapping.tsv | grep -v "Network Object") > ${OUTPUT_FOLDER}genego_${tag}_mapping_refined.tsv
echo "GeneGo ids not found in Geneder data:" >> ${OUTPUT_FOLDER}genego_${tag}_mapping_help.txt
join -t $'\t' -v 2 -1 1 <(cut -f 1  ${INPUT_FOLDER}*rankings.tsv | grep -v Gene | sort -u -k1,1) -2 2 <(sort -t $'\t' -k2,2 ${OUTPUT_FOLDER}genego_${tag}_mapping.tsv | grep -v "Network Object") >> ${OUTPUT_FOLDER}genego_${tag}_mapping_help.txt

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
