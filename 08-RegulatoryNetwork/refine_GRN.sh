#!/bin/bash -l
#SBATCH -J geneder:08:refine
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 2
#SBATCH --time=0-00:05:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
RAW_INPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Original/Else/DoRothEA/
INPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/07/
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/08/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/08-RegulatoryNetwork/

# Copy the funkiapp files
cp ${RAW_INPUT_FOLDER}/SNage* ${OUTPUT_FOLDER}

# Actual job in bash.
# First, DoRothEA files, we need to loop over relevant GRN files.
rm -rf ${OUTPUT_FOLDER}*_dorothea_GRN_*_refined.tsv
FILES=${OUTPUT_FOLDER}*_dorothea_GRN_*
for f in $FILES
do
  f2=${f/\.tsv/_refined.tsv}

  # First, we map the DoRothEA data, we add female diff data and then male diff data.
  # [Female] We start by mapping the second field, which then moves first.
  join -t $'\t' -a 1 -e "NA" -o auto -1 2 <(sort -t $'\t' -k2,2 ${f}) -2 1 <(sort -t $'\t' -k1,1 ${INPUT_FOLDER}SNage_PDVsControl_females_max-avg_all_pivalue_rankings.tsv) | cut -f 1-5,6,8 > ${OUTPUT_FOLDER}File_temp.tsv

  # [Female] Then, we map the first field, which is now located second and is moved back to first place.
  join -t $'\t' -a 1 -e "NA" -o auto -1 2 <(sort -t $'\t' -k2,2 ${OUTPUT_FOLDER}File_temp.tsv) -2 1 <(sort -t $'\t' -k1,1 ${INPUT_FOLDER}SNage_PDVsControl_females_max-avg_all_pivalue_rankings.tsv) | cut -f 1-7,8,10 > ${OUTPUT_FOLDER}File_temp2.tsv
  mv ${OUTPUT_FOLDER}File_temp2.tsv ${OUTPUT_FOLDER}File_temp.tsv

  # [Male] We continue by mapping the second field again (now for male), which then moves first.
  join -t $'\t' -a 1 -e "NA" -o auto -1 2 <(sort -t $'\t' -k2,2 ${OUTPUT_FOLDER}File_temp.tsv) -2 1 <(sort -t $'\t' -k1,1 ${INPUT_FOLDER}SNage_PDVsControl_males_max-avg_all_pivalue_rankings.tsv) | cut -f 1-9,10,12 > ${OUTPUT_FOLDER}File_temp2.tsv
  mv ${OUTPUT_FOLDER}File_temp2.tsv ${OUTPUT_FOLDER}File_temp.tsv

  # [Male] Then, we map the first field, which is again located second and is moved back to first place.
  join -t $'\t' -a 1 -e "NA" -o auto -1 2 <(sort -t $'\t' -k2,2 ${OUTPUT_FOLDER}File_temp.tsv) -2 1 <(sort -t $'\t' -k1,1 ${INPUT_FOLDER}SNage_PDVsControl_males_max-avg_all_pivalue_rankings.tsv) | cut -f 1-11,12,14 > ${OUTPUT_FOLDER}File_temp2.tsv
  paste <(head -n 1 ${OUTPUT_FOLDER}Dorothea_regulons_clean.tsv) <(echo $'Female.TF.logFC\tFemale.TF.adjPval\tFemale.target.logFC\tFemale.target.adjPval\tMale.TF.logFC\tMale.TF.adjPval\tMale.target.logFC\tMale.target.adjPval') > ${f2}
  paste <(cut -f 1-5,8-9 ${OUTPUT_FOLDER}File_temp2.tsv) <(cut -f 6-7,12-13 ${OUTPUT_FOLDER}File_temp2.tsv) <(cut -f 10-11 ${OUTPUT_FOLDER}File_temp2.tsv) >> ${f2}
  rm ${OUTPUT_FOLDER}File_temp*.tsv
done

# Then, Funkiapp file (only one so far).
rm -rf ${OUTPUT_FOLDER}*_funkiapp_GRN_*_refined.tsv
FILES=${OUTPUT_FOLDER}*_funkiapp_GRN_*
for f in $FILES
do
  f2=${f/\.tsv/_refined.tsv}

  # First, we map the DoRothEA data, we add female diff data and then male diff data.
  # [Female] We start by mapping the second field, which then moves first.
  join -t $'\t' -a 1 -e "NA" -o auto -1 2 <(sort -t $'\t' -k2,2 ${f}) -2 1 <(sort -t $'\t' -k1,1 ${INPUT_FOLDER}SNage_PDVsControl_females_max-avg_all_pivalue_rankings.tsv) | cut -f 1-5,6,8 > ${OUTPUT_FOLDER}File_temp.tsv

  # [Female] Then, we map the first field, which is now located second and is moved back to first place.
  join -t $'\t' -a 1 -e "NA" -o auto -1 2 <(sort -t $'\t' -k2,2 ${OUTPUT_FOLDER}File_temp.tsv) -2 1 <(sort -t $'\t' -k1,1 ${INPUT_FOLDER}SNage_PDVsControl_females_max-avg_all_pivalue_rankings.tsv) | cut -f 1-7,8,10 > ${OUTPUT_FOLDER}File_temp2.tsv
  mv ${OUTPUT_FOLDER}File_temp2.tsv ${OUTPUT_FOLDER}File_temp.tsv

  # [Male] We continue by mapping the second field again (now for male), which then moves first.
  join -t $'\t' -a 1 -e "NA" -o auto -1 2 <(sort -t $'\t' -k2,2 ${OUTPUT_FOLDER}File_temp.tsv) -2 1 <(sort -t $'\t' -k1,1 ${INPUT_FOLDER}SNage_PDVsControl_males_max-avg_all_pivalue_rankings.tsv) | cut -f 1-9,10,12 > ${OUTPUT_FOLDER}File_temp2.tsv
  mv ${OUTPUT_FOLDER}File_temp2.tsv ${OUTPUT_FOLDER}File_temp.tsv

  # [Male] Then, we map the first field, which is again located second and is moved back to first place.
  join -t $'\t' -a 1 -e "NA" -o auto -1 2 <(sort -t $'\t' -k2,2 ${OUTPUT_FOLDER}File_temp.tsv) -2 1 <(sort -t $'\t' -k1,1 ${INPUT_FOLDER}SNage_PDVsControl_males_max-avg_all_pivalue_rankings.tsv) | cut -f 1-11,12,14 > ${OUTPUT_FOLDER}File_temp2.tsv
  paste <(head -n 1 ${OUTPUT_FOLDER}Dorothea_regulons_clean.tsv) <(echo $'Female.TF.logFC\tFemale.TF.adjPval\tFemale.target.logFC\tFemale.target.adjPval\tMale.TF.logFC\tMale.TF.adjPval\tMale.target.logFC\tMale.target.adjPval') > ${f2}
  paste <(cut -f 1-5,8-9 ${OUTPUT_FOLDER}File_temp2.tsv) <(cut -f 6-7,12-13 ${OUTPUT_FOLDER}File_temp2.tsv) <(cut -f 10-11 ${OUTPUT_FOLDER}File_temp2.tsv) >> ${f2}
  rm ${OUTPUT_FOLDER}File_temp*.tsv
done

# Now, we take care of GeneGo.
# We first create the ALL_lists mapping by simple concatenation (was not done before since we were waiting for the manual curation to take place).
cat ${OUTPUT_FOLDER}SNage_PDVsControl_both_max-avg_all_Pvalue_rankings_genes_GRN_genego_mapping_refined.tsv ${OUTPUT_FOLDER}genego_extended_newTFs_mapping_refined.tsv | sort -u > ${OUTPUT_FOLDER}genego_mapping_refined.tsv

# Second, GeneGO files, we need to loop over relevant GRN files again.
# Note, we also do the extended networks despite the fact that we did not have the geneGo ids for all the new genes that GeneGo includes.
# These were obtained via an additional GeneGo network query.
rm -rf ${OUTPUT_FOLDER}*_GRN_genego_*_refined.tsv
FILES=${OUTPUT_FOLDER}*_GRN_genego_internal*
for f in $FILES
do
  f2=${f/\.tsv/_refined.tsv}

  # Contrary to the DoRothEA case, we first need to map the GeneGO ids to GeneDER ids, then we will be able to 
  # proceed with actually getting the differential expression data.
  # We first map the target id (on position 4, will move to position 1).
  join -t $'\t' -o auto -1 4 <(sort -t $'\t' -k4,4 ${f} | grep -v "Network Object") -2 2 <(sort -t $'\t' -k2,2 ${OUTPUT_FOLDER}genego_mapping_refined.tsv) > ${OUTPUT_FOLDER}File_temp.tsv

  # Then, we map the TF, which is now located third and is moved to first place.
  join -t $'\t' -o auto -1 3 <(sort -t $'\t' -k3,3 ${OUTPUT_FOLDER}File_temp.tsv) -2 2 <(sort -t $'\t' -k2,2 ${OUTPUT_FOLDER}genego_mapping_refined.tsv) > ${OUTPUT_FOLDER}File_temp2.tsv
  paste <(cut -f 12 ${OUTPUT_FOLDER}File_temp2.tsv) <(cut -f 11 ${OUTPUT_FOLDER}File_temp2.tsv) <(cut -f 1-10 ${OUTPUT_FOLDER}File_temp2.tsv) > ${OUTPUT_FOLDER}File_temp.tsv
  rm ${OUTPUT_FOLDER}File_temp2.tsv

  # We then add female diff data and then male diff data.
  # [Female] We start by mapping the second field, which then moves first.
  join -t $'\t' -a 1 -e "NA" -o auto -1 2 <(sort -t $'\t' -k2,2 ${OUTPUT_FOLDER}File_temp.tsv) -2 1 <(sort -t $'\t' -k1,1 ${INPUT_FOLDER}SNage_PDVsControl_females_max-avg_all_pivalue_rankings.tsv) | cut -f 1-12,13,15 > ${OUTPUT_FOLDER}File_temp2.tsv
  mv ${OUTPUT_FOLDER}File_temp2.tsv ${OUTPUT_FOLDER}File_temp.tsv

  # [Female] Then, we map the first field, which is now located second and is moved back to first place.
  join -t $'\t' -a 1 -e "NA" -o auto -1 2 <(sort -t $'\t' -k2,2 ${OUTPUT_FOLDER}File_temp.tsv) -2 1 <(sort -t $'\t' -k1,1 ${INPUT_FOLDER}SNage_PDVsControl_females_max-avg_all_pivalue_rankings.tsv) | cut -f 1-14,15,17 > ${OUTPUT_FOLDER}File_temp2.tsv
  mv ${OUTPUT_FOLDER}File_temp2.tsv ${OUTPUT_FOLDER}File_temp.tsv

  # [Male] We continue by mapping the second field again (now for male), which then moves first.
  join -t $'\t' -a 1 -e "NA" -o auto -1 2 <(sort -t $'\t' -k2,2 ${OUTPUT_FOLDER}File_temp.tsv) -2 1 <(sort -t $'\t' -k1,1 ${INPUT_FOLDER}SNage_PDVsControl_males_max-avg_all_pivalue_rankings.tsv) | cut -f 1-16,17,19 > ${OUTPUT_FOLDER}File_temp2.tsv
  mv ${OUTPUT_FOLDER}File_temp2.tsv ${OUTPUT_FOLDER}File_temp.tsv

  # [Male] Then, we map the first field, which is again located second and is moved back to first place.
  join -t $'\t' -a 1 -e "NA" -o auto -1 2 <(sort -t $'\t' -k2,2 ${OUTPUT_FOLDER}File_temp.tsv) -2 1 <(sort -t $'\t' -k1,1 ${INPUT_FOLDER}SNage_PDVsControl_males_max-avg_all_pivalue_rankings.tsv) | cut -f 1-18,19,21 > ${OUTPUT_FOLDER}File_temp2.tsv

  echo $'Source\tTarget\tSource.ggid\tTarget.ggid\tggid\tSource.type\tTarget.type\tRegulation\tCategory\tSpecies\tInformation\tReferences\tFemale.TF.logFC\tFemale.TF.adjPval\tFemale.target.logFC\tFemale.target.adjPval\tMale.TF.logFC\tMale.TF.adjPval\tMale.target.logFC\tMale.target.adjPval' > ${f2}
  paste <(cut -f 1-12,15-16 ${OUTPUT_FOLDER}File_temp2.tsv) <(cut -f 13-14,19-20 ${OUTPUT_FOLDER}File_temp2.tsv) <(cut -f 17-18 ${OUTPUT_FOLDER}File_temp2.tsv) >> ${f2}
  rm ${OUTPUT_FOLDER}File_temp*.tsv
done

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
