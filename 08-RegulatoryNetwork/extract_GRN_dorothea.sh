#!/bin/bash -l
#SBATCH -J geneder:08:extract
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 2
#SBATCH --time=0-00:05:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
DATA_FOLDER=/home/users/ltranchevent/Data/GeneDER/Original/Else/DoRothEA/
INPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/07/
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/08/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/08-RegulatoryNetwork/

# We first use the manually created file "TFactivities_merged_selected_consistent.tsv"
# which contains the 24 TFs (in v08) that are present in the selected_consistent sheet
# of the TFactivity_merged spreadsheet.
# The rationale is that these genes are TFs that could be important but are not
# necessarily differentially expressed themselves and are therefore not already
# present in the "SNage_all_max-avg_all_lists_genes.tsv" file. We add them as
# to collect the interactions for these genes.
cp ${DATA_FOLDER}tf_activities_both_ABC_raw.csv ${OUTPUT_FOLDER}
cat <(cat ${OUTPUT_FOLDER}tf_activities_both_ABC_raw.csv | cut -f 1 -d ',' | sort -u | grep -v 'tf') ${OUTPUT_FOLDER}SNage_all_max-avg_all_lists_genes.tsv | sort -u > ${OUTPUT_FOLDER}FTEMP
mv ${OUTPUT_FOLDER}FTEMP ${OUTPUT_FOLDER}SNage_all_max-avg_all_lists_genes.tsv

# Actual job in bash. This is a simple join between the TF-targets link database and the experimental data (differentially expressed genes).

# We update the database based on the DoRothEA_unmapped_mapped_clean.tsv file.
cp ${OUTPUT_FOLDER}Dorothea_regulons_clean.tsv ${OUTPUT_FOLDER}Dorothea_temp.tsv
mv ${OUTPUT_FOLDER}Dorothea_regulons_clean.tsv ${OUTPUT_FOLDER}Dorothea_regulons_clean_wrongids.tsv
input=${OUTPUT_FOLDER}Dorothea_geneids_unmapped_mapped_clean.tsv
while IFS=$'\t' read -r c1 c2
do
  sed -r 's/^'"${c1}"'\t/'"${c2}"'\t/g' ${OUTPUT_FOLDER}Dorothea_temp.tsv > ${OUTPUT_FOLDER}Dorothea_temp2.tsv
  mv  ${OUTPUT_FOLDER}Dorothea_temp2.tsv ${OUTPUT_FOLDER}Dorothea_temp.tsv
  sed -r 's/\t'"$c1"'\t/\t'"$c2"'\t/g' ${OUTPUT_FOLDER}Dorothea_temp.tsv > ${OUTPUT_FOLDER}Dorothea_temp2.tsv
  mv ${OUTPUT_FOLDER}Dorothea_temp2.tsv ${OUTPUT_FOLDER}Dorothea_temp.tsv
done < "$input"
mv ${OUTPUT_FOLDER}Dorothea_temp.tsv ${OUTPUT_FOLDER}Dorothea_regulons_clean.tsv

# First the female data.
join -1 1 -2 2 <(sort -k1,1 ${OUTPUT_FOLDER}SNage_PDVsControl_females_max-avg_all_Pvalue_rankings_genes.tsv) <(sort -k2,2 ${OUTPUT_FOLDER}Dorothea_regulons_clean.tsv) > ${OUTPUT_FOLDER}SNage_PDVsControl_females_dorothea_GRN_extended_raw.tsv
paste <(cut -f 2 -d ' ' ${OUTPUT_FOLDER}SNage_PDVsControl_females_dorothea_GRN_extended_raw.tsv) <(cut -f 1 -d ' ' ${OUTPUT_FOLDER}SNage_PDVsControl_females_dorothea_GRN_extended_raw.tsv) <(cut -f 3- -d ' ' ${OUTPUT_FOLDER}SNage_PDVsControl_females_dorothea_GRN_extended_raw.tsv) | sed -r 's/ /\t/g' > ${OUTPUT_FOLDER}SNage_PDVsControl_females_dorothea_GRN_extended.tsv
rm ${OUTPUT_FOLDER}SNage_PDVsControl_females_dorothea_GRN_extended_raw.tsv
join -1 1 -2 1 <(sort -k1,1 ${OUTPUT_FOLDER}SNage_PDVsControl_females_max-avg_all_Pvalue_rankings_genes.tsv) <(sort -k1,1 ${OUTPUT_FOLDER}SNage_PDVsControl_females_dorothea_GRN_extended.tsv) | sed -r 's/ /\t/g' > ${OUTPUT_FOLDER}SNage_PDVsControl_females_dorothea_GRN_internal.tsv

# Second the male data.
join -1 1 -2 2 <(sort -k1,1 ${OUTPUT_FOLDER}SNage_PDVsControl_males_max-avg_all_Pvalue_rankings_genes.tsv) <(sort -k2,2 ${OUTPUT_FOLDER}Dorothea_regulons_clean.tsv) > ${OUTPUT_FOLDER}SNage_PDVsControl_males_dorothea_GRN_extended_raw.tsv
paste <(cut -f 2 -d ' ' ${OUTPUT_FOLDER}SNage_PDVsControl_males_dorothea_GRN_extended_raw.tsv) <(cut -f 1 -d ' ' ${OUTPUT_FOLDER}SNage_PDVsControl_males_dorothea_GRN_extended_raw.tsv) <(cut -f 3- -d ' ' ${OUTPUT_FOLDER}SNage_PDVsControl_males_dorothea_GRN_extended_raw.tsv) | sed -r 's/ /\t/g' > ${OUTPUT_FOLDER}SNage_PDVsControl_males_dorothea_GRN_extended.tsv
rm ${OUTPUT_FOLDER}SNage_PDVsControl_males_dorothea_GRN_extended_raw.tsv
join -1 1 -2 1 <(sort -k1,1 ${OUTPUT_FOLDER}SNage_PDVsControl_males_max-avg_all_Pvalue_rankings_genes.tsv) <(sort -k1,1 ${OUTPUT_FOLDER}SNage_PDVsControl_males_dorothea_GRN_extended.tsv) | sed -r 's/ /\t/g' > ${OUTPUT_FOLDER}SNage_PDVsControl_males_dorothea_GRN_internal.tsv

# Third, the combination of all files as one.
join -1 1 -2 2 <(sort -k1,1 ${OUTPUT_FOLDER}SNage_all_max-avg_all_lists_genes.tsv) <(sort -k2,2 ${OUTPUT_FOLDER}Dorothea_regulons_clean.tsv) > ${OUTPUT_FOLDER}SNage_all_lists_dorothea_GRN_extended_raw.tsv
paste <(cut -f 2 -d ' ' ${OUTPUT_FOLDER}SNage_all_lists_dorothea_GRN_extended_raw.tsv) <(cut -f 1 -d ' ' ${OUTPUT_FOLDER}SNage_all_lists_dorothea_GRN_extended_raw.tsv) <(cut -f 3- -d ' ' ${OUTPUT_FOLDER}SNage_all_lists_dorothea_GRN_extended_raw.tsv) | sed -r 's/ /\t/g' > ${OUTPUT_FOLDER}SNage_all_lists_dorothea_GRN_extended.tsv
rm ${OUTPUT_FOLDER}SNage_all_lists_dorothea_GRN_extended_raw.tsv
join -1 1 -2 1 <(sort -k1,1 ${OUTPUT_FOLDER}SNage_all_max-avg_all_lists_genes.tsv) <(sort -k1,1 ${OUTPUT_FOLDER}SNage_all_lists_dorothea_GRN_extended.tsv) | sed -r 's/ /\t/g' > ${OUTPUT_FOLDER}SNage_all_lists_dorothea_GRN_internal.tsv

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
