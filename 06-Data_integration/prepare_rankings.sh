#!/bin/bash -l
#SBATCH -J geneder:06:rankings
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --time=0-00:20:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
INPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/05/
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/06/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/06-Data_integration/

# Loading modules.
resif-load-swset-legacy
module load lang/R/3.6.2-foss-2019b-bare

# Additional files that contain the genes that have different probe mappings between the male and female analyses.
# Comment out the next 8 lines for the _b*_ analyses.
echo "Gene" > ${OUTPUT_FOLDER}SNage_PDVsControl_females_vs_males_matching_data_problems.tsv
echo "Gene" > ${OUTPUT_FOLDER}SN_PDVsControl_females_vs_males_matching_data_problems.tsv
echo "Gene" > ${OUTPUT_FOLDER}DA_PDVsControl_females_vs_males_matching_data_problems.tsv
echo "Gene" > ${OUTPUT_FOLDER}iPSC-DA_PDVsControl_females_vs_males_matching_data_problems.tsv
diff <(sort -k3,3 ${OUTPUT_FOLDER}SNage_PDVsControl_females_matching_data.tsv   | cut -f 3,16,19,22,25,28) <(sort -k3,3 ${OUTPUT_FOLDER}SNage_PDVsControl_males_matching_data.tsv   | cut -f 3,16,19,25,28,31) | grep '>' | cut -f 1 | sed -r "s/> //g" >> ${OUTPUT_FOLDER}SNage_PDVsControl_females_vs_males_matching_data_problems.tsv
diff <(sort -k3,3 ${OUTPUT_FOLDER}SN_PDVsControl_females_matching_data.tsv      | cut -f 3,16,19,22,25,28) <(sort -k3,3 ${OUTPUT_FOLDER}SN_PDVsControl_males_matching_data.tsv      | cut -f 3,16,19,25,28,31) | grep '>' | cut -f 1 | sed -r "s/> //g" >> ${OUTPUT_FOLDER}SN_PDVsControl_females_vs_males_matching_data_problems.tsv
diff <(sort -k3,3 ${OUTPUT_FOLDER}DA_PDVsControl_females_matching_data.tsv      | cut -f 3,16,19,22)       <(sort -k3,3 ${OUTPUT_FOLDER}DA_PDVsControl_males_matching_data.tsv      | cut -f 3,16,19,22)       | grep '>' | cut -f 1 | sed -r "s/> //g" >> ${OUTPUT_FOLDER}DA_PDVsControl_females_vs_males_matching_data_problems.tsv
diff <(sort -k3,3 ${OUTPUT_FOLDER}iPSC-DA_PDVsControl_females_matching_data.tsv | cut -f 3,16)             <(sort -k3,3 ${OUTPUT_FOLDER}iPSC-DA_PDVsControl_males_matching_data.tsv | cut -f 3,16)             | grep '>' | cut -f 1 | sed -r "s/> //g" >> ${OUTPUT_FOLDER}iPSC-DA_PDVsControl_females_vs_males_matching_data_problems.tsv

# We refine the FDR values, compute the gender specificity scores and the overlap percentages.
Rscript --vanilla ${CODE_FOLDER}/refine_FDR_values.R          > ${OUTPUT_FOLDER}refine_FDR_log.out      2> ${OUTPUT_FOLDER}refine_FDR_log.err
Rscript --vanilla ${CODE_FOLDER}/compute_gender_specificity.R > ${OUTPUT_FOLDER}gdr_spec_log.out        2> ${OUTPUT_FOLDER}gdr_spec_log.err
Rscript --vanilla ${CODE_FOLDER}/compute_dataset_overlap.R    > ${OUTPUT_FOLDER}dataset_overlap_log.out 2> ${OUTPUT_FOLDER}dataset_overlap_log.err
Rscript --vanilla ${CODE_FOLDER}/merge_and_filter_rankings.R  > ${OUTPUT_FOLDER}merge_filter_log.out    2> ${OUTPUT_FOLDER}merge_filter_log.err
Rscript --vanilla ${CODE_FOLDER}/refine_GD_rankings.R         > ${OUTPUT_FOLDER}refine_rankings_log.out 2> ${OUTPUT_FOLDER}refine_rankings_log.err

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
