#!/bin/bash -l
#SBATCH -J geneder:06:integrate
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 20
#SBATCH --time=0-3:30:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
INPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/05/
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/06/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/06-Data_integration/

# Loading modules.
resif-load-swset-legacy
module load lang/R/3.6.2-foss-2019b-bare

# Load configuration
source ../libs/conf/confSH.sh
create_variables ../Confs/datasets_config.yml

# Preparing the job.
cp -rf ${INPUT_FOLDER}clinical_categories_summarized.tsv ${OUTPUT_FOLDER}clinical_categories_summarized.tsv

# Actual job
Rscript --vanilla ${CODE_FOLDER}integrate_datasets.R > ${OUTPUT_FOLDER}integrate_log.out 2> ${OUTPUT_FOLDER}integrate_log.err

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
