#!/bin/bash -l
#SBATCH -J geneder:06:analyse
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 4
#SBATCH --time=0-2:45:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/06/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/06-Data_integration/

# Loading modules.
resif-load-swset-legacy
module load lang/R/3.6.2-foss-2019b-bare

# Actual jobs
Rscript --vanilla ${CODE_FOLDER}analyse_integration_results.R > ${OUTPUT_FOLDER}analyse_log.out 2> ${OUTPUT_FOLDER}analyse_log.err
Rscript --vanilla ${CODE_FOLDER}analyse_integration_results_figures.R > ${OUTPUT_FOLDER}analyse_fig_log.out 2> ${OUTPUT_FOLDER}analyse_fig_log.err

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}

