#!/bin/bash -l
#SBATCH -J geneder:06:doc
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --time=0-00:15:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# I/Os and parameters
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/06/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/06-Data_integration/

# Load configuration
source ../libs/conf/confSH.sh
create_variables ../Confs/datasets_config.yml
create_variables ../Confs/project_config.yml

# =============================================================================
#
#			PART I
#
# =============================================================================

# Clean start
rm -rf ${OUTPUT_FOLDER}results_summary.*

# Print header
echo '\documentclass[]{article}' > ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\usepackage{graphicx}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\usepackage{booktabs}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\usepackage{pgfplotstable}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\usepackage{rotating}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\title{GeneDER - step 06 - Data integration - Part I}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\author{Leon-Charles Tranchevent}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\begin{document}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\maketitle' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\textsl{}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\begin{abstract}' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'This document summarizes the first results of the step 06-Data\_integration. For each integration' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'scheme and for each limma analysis, the distributions of fold changes and p-values are plotted' >> ${OUTPUT_FOLDER}results_summary.tex
echo '(before data integration). The distributions of relevant fold changes (\textit{i.e.}, to combine)' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'and the correlation bewteen the different datasets are also displayed. After data integration, the correlation' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'between various data integration techniques is shown.\\' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'Note: this document is automatically generated.' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\end{abstract}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\clearpage' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex


# For each integration scheme.
nbSchemes=${#integrations__name[@]}
for (( i=0; i<$nbSchemes; i++ ))
do
	integrationName=${integrations__name[$i]}

	# For each Limma comparison.
	nbAnalyses=${#limma_analyses__name[@]}
	for (( k=0; k<$nbAnalyses; k++ ))
	do
		analysisNameRaw=${limma_analyses__name[$k]}
		analysisName=$(echo $analysisNameRaw | sed -r 's/[\",\[]+//g' | sed -r 's/\]//g')

		# Distributions of fold changes and P values.
		echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \centering' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \includegraphics[scale=0.34]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_fc_dist.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \includegraphics[scale=0.34]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_pval_dist.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \caption{Distributions of fold changes (left) and P values (right) for integration scheme \textit{'"${integrationName}"'}, ' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  and Limma analysis \textit{'"${analysisName}"'}. One plot per dataset.}' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex

		# Distributions of number of relevant fold changes (and therefore number of P values to integrate).
		echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \centering' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \includegraphics[scale=0.34]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_max-avg_relfc_dist.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \includegraphics[scale=0.34]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_best-pval_relfc_dist.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \caption{Distributions of number of relevant fold changes for integration scheme \textit{'"${integrationName}"'}, ' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  and Limma analysis \textit{'"${analysisName}"'}. A fold change is relevant if its sign is the same as the ' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  sign of the median fold change. (Left) Gene probes selected based on the highest average expression (AVG) ' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  (Right) Gene probes selected based on the best P value (PVAL)}.' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex

		# Correlations indicated as tables.
		# Prepare tables for LaTeX.
		sed -r 's/\_//g' ${OUTPUT_FOLDER}${integrationName}_${analysisName}_max-avg_fc_global_correlations.tsv | sed -r 's/\tNA/\t1/g' > ${OUTPUT_FOLDER}${integrationName}_${analysisName}_max-avg_fc_global_correlations_4latex.tsv
		sed -r 's/\_//g' ${OUTPUT_FOLDER}${integrationName}_${analysisName}_max-avg_pval_global_correlations.tsv | sed -r 's/\tNA/\t1/g' > ${OUTPUT_FOLDER}${integrationName}_${analysisName}_max-avg_pval_global_correlations_4latex.tsv
		sed -r 's/\_//g' ${OUTPUT_FOLDER}${integrationName}_${analysisName}_max-avg_fc_top_global_correlations.tsv | sed -r 's/\tNA/\t1/g' > ${OUTPUT_FOLDER}${integrationName}_${analysisName}_max-avg_fc_top_global_correlations_4latex.tsv
		sed -r 's/\_//g' ${OUTPUT_FOLDER}${integrationName}_${analysisName}_max-avg_pval_top_global_correlations.tsv | sed -r 's/\tNA/\t1/g' > ${OUTPUT_FOLDER}${integrationName}_${analysisName}_max-avg_pval_top_global_correlations_4latex.tsv
		sed -r 's/\_//g' ${OUTPUT_FOLDER}${integrationName}_${analysisName}_best-pval_fc_global_correlations.tsv | sed -r 's/\tNA/\t1/g' > ${OUTPUT_FOLDER}${integrationName}_${analysisName}_best-pval_fc_global_correlations_4latex.tsv
		sed -r 's/\_//g' ${OUTPUT_FOLDER}${integrationName}_${analysisName}_best-pval_pval_global_correlations.tsv | sed -r 's/\tNA/\t1/g' > ${OUTPUT_FOLDER}${integrationName}_${analysisName}_best-pval_pval_global_correlations_4latex.tsv
		sed -r 's/\_//g' ${OUTPUT_FOLDER}${integrationName}_${analysisName}_best-pval_fc_top_global_correlations.tsv | sed -r 's/\tNA/\t1/g' > ${OUTPUT_FOLDER}${integrationName}_${analysisName}_best-pval_fc_top_global_correlations_4latex.tsv
		sed -r 's/\_//g' ${OUTPUT_FOLDER}${integrationName}_${analysisName}_best-pval_pval_top_global_correlations.tsv | sed -r 's/\tNA/\t1/g' > ${OUTPUT_FOLDER}${integrationName}_${analysisName}_best-pval_pval_top_global_correlations_4latex.tsv

		# T1: AVG - fc.
		echo '\begin{table}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \begin{center}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    \setlength{\tabcolsep}{0.5\tabcolsep}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    \resizebox{\linewidth}{!}{\pgfplotstabletypeset[' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      col sep=tab,' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      every head row/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        before row={\toprule},' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        after row={\midrule}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      every last row/.style={after row=\bottomrule},' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      display columns/0/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        string type,' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        column type={l}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    ]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_max-avg_fc_global_correlations_4latex.tsv}}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    \caption{Fold change correlations among the datasets for integration scheme \textit{'"${integrationName}"'}, '  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      and Limma analysis \textit{'"${analysisName}"'}. Pearson correlations are in the upper triangle, Spearman rank'  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      correlations are in the lower triangle. NOTE: Gene probes selected based on the highest average expression (AVG).}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex

		# T1: AVG - pval.
		echo '    \resizebox{\linewidth}{!}{\pgfplotstabletypeset[' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      col sep=tab,' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      every head row/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        before row={\toprule},' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        after row={\midrule}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      every last row/.style={after row=\bottomrule},' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      display columns/0/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        string type,' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        column type={l}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    ]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_max-avg_pval_global_correlations_4latex.tsv}}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    \caption{P value correlations among the datasets for integration scheme \textit{'"${integrationName}"'}, '  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      and Limma analysis \textit{'"${analysisName}"'}. P values are transformed using -log10(). Pearson correlations are in the upper triangle, Spearman rank'  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      correlations are in the lower triangle. NOTE: Gene probes selected based on the highest average expression (AVG).}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \end{center}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '\end{table}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex

		# T2: AVG - fc (top only).
		echo '\begin{table}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \begin{center}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    \setlength{\tabcolsep}{0.5\tabcolsep}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    \resizebox{\linewidth}{!}{\pgfplotstabletypeset[' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      col sep=tab,' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      every head row/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        before row={\toprule},' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        after row={\midrule}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      every last row/.style={after row=\bottomrule},' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      display columns/0/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        string type,' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        column type={l}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    ]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_max-avg_fc_top_global_correlations_4latex.tsv}}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    \caption{Fold change correlations among the datasets for integration scheme \textit{'"${integrationName}"'}, '  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      and Limma analysis \textit{'"${analysisName}"'} and restricted to the genes with the best P values only. Pearson correlations are in the upper triangle, Spearman rank'  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      correlations are in the lower triangle. NOTE: Gene probes selected based on the highest average expression (AVG).}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex

		# T2: AVG - pval (top only).
		echo '    \resizebox{\linewidth}{!}{\pgfplotstabletypeset[' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      col sep=tab,' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      every head row/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        before row={\toprule},' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        after row={\midrule}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      every last row/.style={after row=\bottomrule},' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      display columns/0/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        string type,' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        column type={l}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    ]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_max-avg_pval_top_global_correlations_4latex.tsv}}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    \caption{P value correlations among the datasets for integration scheme \textit{'"${integrationName}"'}, '  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      and Limma analysis \textit{'"${analysisName}"'} and restricted to the genes with the best P values only. P values are transformed using -log10(). Pearson correlations are in the upper triangle, Spearman rank'  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      correlations are in the lower triangle. NOTE: Gene probes selected based on the highest average expression (AVG).}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \end{center}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '\end{table}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex

		# T3: PVAL - fc.
		echo '\begin{table}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \begin{center}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    \setlength{\tabcolsep}{0.5\tabcolsep}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    \resizebox{\linewidth}{!}{\pgfplotstabletypeset[' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      col sep=tab,' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      every head row/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        before row={\toprule},' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        after row={\midrule}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      every last row/.style={after row=\bottomrule},' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      display columns/0/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        string type,' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        column type={l}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    ]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_best-pval_fc_global_correlations_4latex.tsv}}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    \caption{Fold change correlations among the datasets for integration scheme \textit{'"${integrationName}"'}, '  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      and Limma analysis \textit{'"${analysisName}"'}. Pearson correlations are in the upper triangle, Spearman rank'  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      correlations are in the lower triangle. NOTE: Gene probes selected based on the best P value (PVAL).}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex

		# T3: PVAL - pval.
		echo '    \resizebox{\linewidth}{!}{\pgfplotstabletypeset[' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      col sep=tab,' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      every head row/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        before row={\toprule},' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        after row={\midrule}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      every last row/.style={after row=\bottomrule},' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      display columns/0/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        string type,' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        column type={l}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    ]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_best-pval_pval_global_correlations_4latex.tsv}}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    \caption{P value correlations among the datasets for integration scheme \textit{'"${integrationName}"'}, '  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      and Limma analysis \textit{'"${analysisName}"'}. P values are transformed using -log10(). Pearson correlations are in the upper triangle, Spearman rank'  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      correlations are in the lower triangle. NOTE: Gene probes selected based on the best P value (PVAL).}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \end{center}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '\end{table}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex

		# T4: PVAL - fc (top only).
		echo '\begin{table}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \begin{center}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    \setlength{\tabcolsep}{0.5\tabcolsep}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    \resizebox{\linewidth}{!}{\pgfplotstabletypeset[' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      col sep=tab,' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      every head row/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        before row={\toprule},' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        after row={\midrule}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      every last row/.style={after row=\bottomrule},' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      display columns/0/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        string type,' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        column type={l}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    ]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_best-pval_fc_top_global_correlations_4latex.tsv}}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    \caption{Fold change correlations among the datasets for integration scheme \textit{'"${integrationName}"'}, '  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      and Limma analysis \textit{'"${analysisName}"'} and restricted to the genes with the best P values only. Pearson correlations are in the upper triangle, Spearman rank'  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      correlations are in the lower triangle. NOTE: Gene probes selected based on the best P value (PVAL).}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex

		# T4: PVAL - pval (top only).
		echo '    \resizebox{\linewidth}{!}{\pgfplotstabletypeset[' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      col sep=tab,' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      every head row/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        before row={\toprule},' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        after row={\midrule}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      every last row/.style={after row=\bottomrule},' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      display columns/0/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        string type,' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '        column type={l}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    ]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_best-pval_pval_top_global_correlations_4latex.tsv}}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '    \caption{P value correlations among the datasets for integration scheme \textit{'"${integrationName}"'}, '  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      and Limma analysis \textit{'"${analysisName}"'} and restricted to the genes with the best P values only. P values are transformed using -log10(). Pearson correlations are in the upper triangle, Spearman rank'  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '      correlations are in the lower triangle. NOTE: Gene probes selected based on the best P value (PVAL).}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \end{center}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '\end{table}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex

		# Comparing AVG and PVAL.
		echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \centering' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \includegraphics[scale=0.40]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_max-avg_vs_best-pval.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \caption{Raw P value correlation between AVG and PVAL seletions for integration scheme \textit{'"${integrationName}"'}, ' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  and Limma analysis \textit{'"${analysisName}"'}. The genes for which both methods lead to the same probe are not plotted (they would be on the diagonal).' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  Correlations are computed for the whole set and only for the subset of genes for which both methods give different results. }' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex

		# Scatter plots of adjusted P values (AVG).
		echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \centering' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_max-avg_marotmayer_vs_ztrans.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_max-avg_marotmayer_vs_fisher.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_max-avg_marotmayer_vs_median.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \caption{P value correlation for integration scheme \textit{'"${integrationName}"'}, ' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  and Limma analysis \textit{'"${analysisName}"'}. Each plot is divided in subplots based on the number' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  of P values combined (from 1 to \textit{n}). (Left) Marot-Mayer adjusted P values against Z-transform adjusted P values.' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  (Center) Marot-Mayer adjusted P values against Fisher adjusted P values.' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  (Right) Marot-Mayer raw P values against the median of raw P values.' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  NOTE: Gene probes selected based on the highest average expression (AVG). }' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex

		# Scatter plots of adjusted P values (PVAL).
		echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \centering' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_best-pval_marotmayer_vs_ztrans.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_best-pval_marotmayer_vs_fisher.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_best-pval_marotmayer_vs_median.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \caption{P value correlation for integration scheme \textit{'"${integrationName}"'}, ' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  and Limma analysis \textit{'"${analysisName}"'}. Each plot is divided in subplots based on the number' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  of P values combined (from 1 to \textit{n}). (Left) Marot-Mayer adjusted P values against Z-transform adjusted P values.' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  (Center) Marot-Mayer adjusted P values against Fisher adjusted P values.' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  (Right) Marot-Mayer raw P values against the median of raw P values.' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  NOTE: Gene probes selected based on the best P value (PVAL). }' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '\clearpage' >> ${OUTPUT_FOLDER}results_summary.tex
	done
done

# Print footer
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\end{document}' >> ${OUTPUT_FOLDER}results_summary.tex

# Compilation
pdflatex -synctex=1 -interaction=nonstopmode ${OUTPUT_FOLDER}results_summary.tex
mv results_summary.pdf ${OUTPUT_FOLDER}results_summary_a.pdf
rm results_summary*
mv ${OUTPUT_FOLDER}results_summary.tex ${OUTPUT_FOLDER}results_summary_a.tex

# =============================================================================
#
#			PART II
#
# =============================================================================

# Clean start
rm -rf ${OUTPUT_FOLDER}results_summary.*

# Print header
echo '\documentclass[]{article}' > ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\usepackage{graphicx}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\usepackage{booktabs}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\usepackage{pgfplotstable}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\usepackage{rotating}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\title{GeneDER - step 06 - Data integration - Part II}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\author{Leon-Charles Tranchevent}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\begin{document}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\maketitle' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\textsl{}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\begin{abstract}' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'This document summarizes the last results of the step 06-Data\_integration. In this document, ' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'the significant genes are counted for each configuration and overlaps between the different' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'configurations are quantified. \\' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'Note: this document is automatically generated.' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\end{abstract}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\clearpage' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex

# Prepare tables for LaTeX.
#grep -v '^SN-' ${OUTPUT_FOLDER}all_counts.tsv | grep -v '^DA-'    > ${OUTPUT_FOLDER}all_counts_SNage_4latex.tsv
#grep -v '^SNage-' ${OUTPUT_FOLDER}all_counts.tsv | grep -v '^DA-' > ${OUTPUT_FOLDER}all_counts_SN_4latex.tsv
#grep -v '^SN-' ${OUTPUT_FOLDER}all_counts.tsv | grep -v '^SNage-' > ${OUTPUT_FOLDER}all_counts_DA_4latex.tsv

grep '^SNage-'   ${OUTPUT_FOLDER}all_counts.tsv > ${OUTPUT_FOLDER}all_counts_SNage_4latex.tsv
grep '^SN-'      ${OUTPUT_FOLDER}all_counts.tsv > ${OUTPUT_FOLDER}all_counts_SN_4latex.tsv
grep '^DA-'      ${OUTPUT_FOLDER}all_counts.tsv > ${OUTPUT_FOLDER}all_counts_DA_4latex.tsv
grep '^iPSC-DA-' ${OUTPUT_FOLDER}all_counts.tsv > ${OUTPUT_FOLDER}all_counts_iPSC-DA_4latex.tsv


# All count table for SNage.
echo '\begin{table}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '  \begin{center}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '    \setlength{\tabcolsep}{0.5\tabcolsep}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '    \resizebox{\linewidth}{!}{\pgfplotstabletypeset[' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      col sep=tab,' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      every head row/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
echo '        before row={\toprule},' >> ${OUTPUT_FOLDER}results_summary.tex
echo '        after row={\midrule}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      every last row/.style={after row=\bottomrule},' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      display columns/0/.style={string type,column type={l}},' >> ${OUTPUT_FOLDER}results_summary.tex
echo '    ]{'"$OUTPUT_FOLDER"'all_counts_SNage_4latex.tsv}}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '    \caption{Numbers of significant genes for each configuration (tissue = SN, age = TRUE).}'  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
echo '  \end{center}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\end{table}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex

# All count table for SN.
echo '\begin{table}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '  \begin{center}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '    \setlength{\tabcolsep}{0.5\tabcolsep}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '    \resizebox{\linewidth}{!}{\pgfplotstabletypeset[' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      col sep=tab,' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      every head row/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
echo '        before row={\toprule},' >> ${OUTPUT_FOLDER}results_summary.tex
echo '        after row={\midrule}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      every last row/.style={after row=\bottomrule},' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      display columns/0/.style={string type,column type={l}},' >> ${OUTPUT_FOLDER}results_summary.tex
echo '    ]{'"$OUTPUT_FOLDER"'all_counts_SN_4latex.tsv}}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '    \caption{Numbers of significant genes for each configuration (tissue = SN).}'  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
echo '  \end{center}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\end{table}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex

# All count table for DA.
echo '\begin{table}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '  \begin{center}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '    \setlength{\tabcolsep}{0.5\tabcolsep}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '    \resizebox{\linewidth}{!}{\pgfplotstabletypeset[' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      col sep=tab,' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      every head row/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
echo '        before row={\toprule},' >> ${OUTPUT_FOLDER}results_summary.tex
echo '        after row={\midrule}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      every last row/.style={after row=\bottomrule},' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      display columns/0/.style={string type,column type={l}},' >> ${OUTPUT_FOLDER}results_summary.tex
echo '    ]{'"$OUTPUT_FOLDER"'all_counts_DA_4latex.tsv}}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '    \caption{Numbers of significant genes for each configuration (tissue = DA).}'  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
echo '  \end{center}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\end{table}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex

# All count table for iPSC-DA
echo '\begin{table}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '  \begin{center}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '    \setlength{\tabcolsep}{0.5\tabcolsep}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '    \resizebox{\linewidth}{!}{\pgfplotstabletypeset[' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      col sep=tab,' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      every head row/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
echo '        before row={\toprule},' >> ${OUTPUT_FOLDER}results_summary.tex
echo '        after row={\midrule}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      every last row/.style={after row=\bottomrule},' >> ${OUTPUT_FOLDER}results_summary.tex
echo '      display columns/0/.style={string type,column type={l}},' >> ${OUTPUT_FOLDER}results_summary.tex
echo '    ]{'"$OUTPUT_FOLDER"'all_counts_iPSC-DA_4latex.tsv}}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '    \caption{Numbers of significant genes for each configuration (tissue = iPSC-DA).}'  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
echo '  \end{center}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\end{table}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex

# For each integration scheme.
nbSchemes=${#integrations__name[@]}
for (( i=0; i<$nbSchemes; i++ ))
do
	integrationName=${integrations__name[$i]}

	# For each Limma analysis.
	nbAnalyses=${#limma_analyses__name[@]}
	for (( k=0; k<$nbAnalyses; k++ ))
	do
		analysisNameRaw=${limma_analyses__name[$k]}
		analysisName=$(echo $analysisNameRaw | sed -r 's/[\",\[]+//g' | sed -r 's/\]//g')

        # Overlap table.
        echo '\begin{table}' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '  \begin{center}' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '    \setlength{\tabcolsep}{0.5\tabcolsep}' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '    \resizebox{\linewidth}{!}{\pgfplotstabletypeset[' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '      col sep=tab,' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '      every head row/.style={' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '        before row={\toprule},' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '        after row={\midrule}' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '      },' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '      every last row/.style={after row=\bottomrule},' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '      display columns/0/.style={string type,column type={l}},' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '      display columns/1/.style={string type,column type={r}},' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '      display columns/2/.style={string type,column type={r}},' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '      display columns/3/.style={string type,column type={r}},' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '      display columns/4/.style={string type,column type={r}},' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '      display columns/5/.style={string type,column type={r}},' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '      display columns/6/.style={string type,column type={r}}' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '    ]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_overlap.tsv}}' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '    \caption{Significant gene overlap between the different configurations for integration scheme \textit{'"${integrationName}"'}, '  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '      and Limma analysis \textit{'"${analysisName}"'}. (Upper triangle) Raw counts.'  | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '      (Lower triangle) Percentage of the overlap with respect to the smallest set of the two.' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '      NOTE: Row headers contain the set sizes.}' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '  \end{center}' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '\end{table}' >> ${OUTPUT_FOLDER}results_summary.tex
        echo '' >> ${OUTPUT_FOLDER}results_summary.tex
	done
done
echo '\clearpage' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex

# For each integration scheme.
nbSchemes=${#integrations__name[@]}
for (( i=0; i<$nbSchemes; i++ ))
do
	integrationName=${integrations__name[$i]}

	# For each Limma analysis.
	nbAnalyses=${#limma_analyses__name[@]}
	for (( k=0; k<$nbAnalyses; k++ ))
	do
		analysisNameRaw=${limma_analyses__name[$k]}
		analysisName=$(echo $analysisNameRaw | sed -r 's/[\",\[]+//g' | sed -r 's/\]//g')

		# Use counts.
		echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
   		echo '  \centering' >> ${OUTPUT_FOLDER}results_summary.tex
   		echo '  \includegraphics[scale=0.34]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_max-avg_use_counts.png}' >> ${OUTPUT_FOLDER}results_summary.tex
   		echo '  \includegraphics[scale=0.34]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_best-pval_use_counts.png}' >> ${OUTPUT_FOLDER}results_summary.tex
   		echo '  \caption{Dataset use counts for integration scheme \textit{'"${integrationName}"'}, ' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
   		echo '  and Limma analysis \textit{'"${analysisName}"'}. Each bar represents the number of times each dataset was used' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
   		echo '  for integration (only when the corresponding fold change is of the same sign than the median fold' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
   		echo '  change across datasets). (Left) Gene probes selected based on the highest average expression (AVG).' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
   		echo '  (Right) Gene probes selected based on the best P value (PVAL).}' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
   		echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
   		echo '' >> ${OUTPUT_FOLDER}results_summary.tex

		# Up vs Down bias (via ECDFs).
		if [ -f ${OUTPUT_FOLDER}${integrationName}_${analysisName}_max-avg_marotmayer_eCDFs.png ]
		then
			if [ -f ${OUTPUT_FOLDER}${integrationName}_${analysisName}_max-avg_fisher_eCDFs.png ]
			then
				if [ -f ${OUTPUT_FOLDER}${integrationName}_${analysisName}_max-avg_ztrans_eCDFs.png ]
				then
					echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
			   		echo '  \centering' >> ${OUTPUT_FOLDER}results_summary.tex
			   		echo '  \includegraphics[scale=0.34]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_max-avg_marotmayer_eCDFs.png}' >> ${OUTPUT_FOLDER}results_summary.tex
			   		echo '  \includegraphics[scale=0.34]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_best-pval_marotmayer_eCDFs.png}' >> ${OUTPUT_FOLDER}results_summary.tex
			   		echo '  \includegraphics[scale=0.34]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_max-avg_fisher_eCDFs.png}' >> ${OUTPUT_FOLDER}results_summary.tex
			   		echo '  \includegraphics[scale=0.34]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_best-pval_fisher_eCDFs.png}' >> ${OUTPUT_FOLDER}results_summary.tex
			   		echo '  \includegraphics[scale=0.34]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_max-avg_ztrans_eCDFs.png}' >> ${OUTPUT_FOLDER}results_summary.tex
			   		echo '  \includegraphics[scale=0.34]{'"$OUTPUT_FOLDER"''"${integrationName}"'_'"${analysisName}"'_best-pval_ztrans_eCDFs.png}' >> ${OUTPUT_FOLDER}results_summary.tex
			   		echo '  \caption{Empirical cumulative distribution functions (ECDF) of P values for integration scheme \textit{'"${integrationName}"'}, ' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
			   		echo '  and Limma analysis \textit{'"${analysisName}"'}. (Top) Marot-Mayer P values. (Center) Fisher P values. (Bottom) Z-transform P values.' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
			   		echo '  (Left) AVG probe-gene selection. (Right) PVAL probe-gene selection. Genes are split and coloured according to whether they are up- or down- regulated.}' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
			   		echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
			   		echo '' >> ${OUTPUT_FOLDER}results_summary.tex
			   	fi
			fi
		fi

        # Top hits for this configuration (AVG).
		LL=(${OUTPUT_FOLDER}${integrationName}_${analysisName}_max-avg_*_geneplot_1_*)
		for l in "${LL[@]}"
		do
			if [[ $l =~ "png" ]]
			then
				echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
        		echo '  \centering' >> ${OUTPUT_FOLDER}results_summary.tex
        		echo '  \includegraphics[scale=0.42]{'"$l"'}' >> ${OUTPUT_FOLDER}results_summary.tex
        		echo '  \caption{Top hit for integration scheme \textit{'"${integrationName}"'}, ' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
        		echo '  and Limma analysis \textit{'"${analysisName}"'}. Expression values are plotted for each dataset (subplots)' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
        		echo '  and for each relevant clinical descriptor (colours).' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
        		echo '  NOTE: Gene probes selected based on the highest average expression (AVG).}' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
        		echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
        		echo '' >> ${OUTPUT_FOLDER}results_summary.tex
        	fi
		done

        # Top hits for this configuration (PVAL).
		LL=(${OUTPUT_FOLDER}${integrationName}_${analysisName}_best-pval_*_geneplot_1_*)
		for l in "${LL[@]}"
		do
			if [[ $l =~ "png" ]]
			then
				echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
        		echo '  \centering' >> ${OUTPUT_FOLDER}results_summary.tex
        		echo '  \includegraphics[scale=0.42]{'"$l"'}' >> ${OUTPUT_FOLDER}results_summary.tex
        		echo '  \caption{Top hit for integration scheme \textit{'"${integrationName}"'}, ' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
        		echo '  and Limma analysis \textit{'"${analysisName}"'}. Expression values are plotted for each dataset (subplots)' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
        		echo '  and for each relevant clinical descriptor (colours).' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
        		echo '  NOTE: Gene probes selected based on the best P value (PVAL).}' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
        		echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
        		echo '' >> ${OUTPUT_FOLDER}results_summary.tex
        	fi
		done
		echo '\clearpage' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex
	done
done

# Print footer
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\end{document}' >> ${OUTPUT_FOLDER}results_summary.tex

# Compilation
pdflatex -synctex=1 -interaction=nonstopmode ${OUTPUT_FOLDER}results_summary.tex
mv results_summary.pdf ${OUTPUT_FOLDER}results_summary_b.pdf
rm results_summary*
mv ${OUTPUT_FOLDER}results_summary.tex ${OUTPUT_FOLDER}results_summary_b.tex

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}

# Clean the temporary LaTeX files.
rm -rf ${OUTPUT_FOLDER}*_4latex*