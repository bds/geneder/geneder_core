# Objectives
The objectives of this step is to perform the meta-analysis, *i.e.* to integrate the results of the differential expression analysis across several datasets in order to identify robust DEGs.

# Details and instructions
The datasets are first summarized at the gene level (limma analyses are performed at the probe level). Conflicts and non unique mappings are handled to create a unique list of DEGS (per dataset still).
```
make clean_outputs
make summarize
```
The results are lists of DEGs (instead of differentially expressed probes) with NA for the genes that are not present in some of the datasets.

The integration itself is then computed and results are analyzed and checked.
```
make integrate
make analyse
make check
```

We create the final gene expression matrices.
```
make gexpr
```

A document that contains the main figures (but not all) can then be generated.
```
make doc
```

Finally, the gene lists to be further analyzed are created. The idea there is to split the sex-specific genes (for both males and females) and the sex-dimorphic genes. We also create various additional figures (heatmaps from the omcis matrices, biomarker boxplots per dataset and sample category of interest).
```
make rankings
make figures
```

# Prerequisites
A prerequisite is to have the results of the limma analysis for all datasets (Step 05).
