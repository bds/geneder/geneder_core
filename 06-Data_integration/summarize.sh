#!/bin/bash -l
#SBATCH -J geneder:06:summarize
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 20
#SBATCH --time=1-02:00:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/06/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/06-Data_integration/
REF=${1}

# Loading modules.
resif-load-swset-legacy
module load lang/R/3.6.2-foss-2019b-bare

# Load configuration
source ../libs/conf/confSH.sh
create_variables ../Confs/project_config.yml

nbSchemes=${#integrations__name[@]}
for (( i=0; i<$nbSchemes; i++ ))
do
	integrationName=${integrations__name[$i]}
	if [ "${integrationName}" == "${REF}" ]
	then
		echo "== Job $i started (${integrationName}) =="
		Rscript --vanilla ${CODE_FOLDER}summarize_gene_level.R ${integrationName} > ${OUTPUT_FOLDER}summarize_${REF}_log.out 2> ${OUTPUT_FOLDER}summarize_${REF}_log.err
		echo "== Job $i ended (${integrationName}) =="
	else
		echo "== Job $i not considered (${integrationName}) =="
	fi
done

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
