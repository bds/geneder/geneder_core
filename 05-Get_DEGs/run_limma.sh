#!/bin/bash -l
#SBATCH -J geneder:05:limma
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 4
#SBATCH --time=0-02:00:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
INPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/04/
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/05/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/05-Get_DEGs/

# Loading modules.
resif-load-swset-legacy
module load lang/R/3.6.2-foss-2019b-bare

# Actual job.
Rscript --vanilla ${CODE_FOLDER}get_DEGs.R          > ${OUTPUT_FOLDER}run_log.out    2> ${OUTPUT_FOLDER}run_log.err
Rscript --vanilla ${CODE_FOLDER}get_PMI-RIN_genes.R > ${OUTPUT_FOLDER}pmirin_log.out 2> ${OUTPUT_FOLDER}pmirin_log.err

# We also copy the sample counts per category and per dataset to the output folder for the next step.
cp ${INPUT_FOLDER}clinical_categories_summarized.tsv ${OUTPUT_FOLDER}clinical_categories_summarized.tsv

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
