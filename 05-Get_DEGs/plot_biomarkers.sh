#!/bin/bash -l
#SBATCH -J geneder:05:plotbm
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 2
#SBATCH --time=0-00:12:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/05/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/05-Get_DEGs/

# Loading modules.
resif-load-swset-legacy
module load lang/R/3.6.2-foss-2019b-bare

# Actual jobs.
Rscript --vanilla ${CODE_FOLDER}plot_biomarkers.R > ${OUTPUT_FOLDER}plot_biomarkers_log.out 2> ${OUTPUT_FOLDER}plot_biomarkers_log.err

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
