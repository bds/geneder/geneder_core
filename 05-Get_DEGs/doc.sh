#!/bin/bash -l
#SBATCH -J geneder:05:doc
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --time=0-00:10:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# I/Os and parameters
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/05/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/05-Get_DEGs/

# Load configuration
source ../libs/conf/confSH.sh
create_variables ../Confs/datasets_config.yml
create_variables ../Confs/project_config.yml

# Clean start
rm -rf ${OUTPUT_FOLDER}results_summary.*

# Print header
echo '\documentclass[]{article}' > ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\usepackage{graphicx}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\title{GeneDER - step 05 - Get DEGs}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\author{Leon-Charles Tranchevent}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\begin{document}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\maketitle' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\textsl{}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\begin{abstract}' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'This document summarizes the results of the step 05-Get\_DEGs. Various plots' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'related to the Limma analyses are displayed for all datasets. The MDS plots aim at ' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'identifying whether there could still be a bias in the dataset (after pre-processing).' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'The MD plots represent the Mean versus the deviation globally for each comparison (significant genes' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'are highlighted in blue/red). The Venn diagrams indicate the overlap between related analyses' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'in terms of shared significant genes). \\' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'Note: this document is automatically generated.' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\end{abstract}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\clearpage' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex

# MDS plots.
nbDatasets=${#datasets__dataset_name[@]}
for (( i=0; i<$nbDatasets; i++ ))
do
	datasetName=${datasets__dataset_name[$i]}
	echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	\centering' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"$datasetName"'_mdsplot_VSN.png}' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"$datasetName"'_mdsplot_noVSN.png}' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	\caption{MDS plot of dataset '"$datasetName"' (Left: VSN, Right: noVSN).}' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '' >> ${OUTPUT_FOLDER}results_summary.tex
done

# MB plots and Venn diagrams. 
nbDatasets=${#datasets__dataset_name[@]}
for (( i=0; i<$nbDatasets; i++ ))
do
	datasetName=${datasets__dataset_name[$i]}
	nbVars=${#variance_methods__name[@]}
	for (( j=0; j<$nbVars; j++ ))
	do
		varName=${variance_methods__name[$j]}

		# MD plots.
		echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\centering' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"$datasetName"'_MD_FemaleVsMale_'"$varName"'.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_FemaleVsMale_PD_${varName}.png ]
		then
			echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"$datasetName"'_MD_FemaleVsMale_PD_'"$varName"'.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		fi
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_FemaleVsMale_control_${varName}.png ]
		then
			echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"$datasetName"'_MD_FemaleVsMale_control_'"$varName"'.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		fi
		echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"$datasetName"'_MD_PDVsControl_'"$varName"'.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_PDVsControl_females_${varName}.png ]
		then
			echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"$datasetName"'_MD_PDVsControl_females_'"$varName"'.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		fi
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_PDVsControl_males_${varName}.png ]
		then
			echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"$datasetName"'_MD_PDVsControl_males_'"$varName"'.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		fi
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_Gender_disease_status_${varName}.png ]
		then
			echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"$datasetName"'_MD_Gender_disease_status_'"$varName"'.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		fi
		echo '	\caption{MD plots of all Limma analyses for dataset '"$datasetName"' ('"$varName"' data).' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	Each plot represents the mean signal (x-axis) versus its deviation (y-axis).' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	From top to bottom and from left to right: ' >> ${OUTPUT_FOLDER}results_summary.tex
		p=1
		echo '	('"$p"') Gender based analysis (Females vs males)' >> ${OUTPUT_FOLDER}results_summary.tex
		p=$((p+1))
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_FemaleVsMale_PD_${varName}.png ]
		then
			echo '	('"$p"') Gender based analysis (Females vs males) in PD patients.' >> ${OUTPUT_FOLDER}results_summary.tex
			p=$((p+1))
		fi
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_FemaleVsMale_control_${varName}.png ]
		then
			echo '	('"$p"') Gender based analysis (Females vs males) in controls.' >> ${OUTPUT_FOLDER}results_summary.tex
			p=$((p+1))
		fi
		echo '	('"$p"') Status based analysis (PD patients vs controls).' >> ${OUTPUT_FOLDER}results_summary.tex
		p=$((p+1))
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_PDVsControl_females_${varName}.png ]
		then
			echo '	('"$p"') Status based analysis (PD patients vs controls) for females.' >> ${OUTPUT_FOLDER}results_summary.tex
			p=$((p+1))
		fi
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_PDVsControl_males_${varName}.png ]
		then
			echo '	('"$p"') Status based analysis (PD patients vs controls) for males.' >> ${OUTPUT_FOLDER}results_summary.tex
			p=$((p+1))
		fi
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_Gender_disease_status_${varName}.png ]
		then
			echo '	('"$p"') Factorial analysis (focus on disease status differences for the different genders).' >> ${OUTPUT_FOLDER}results_summary.tex
			p=$((p+1))
		fi
		echo '	}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex

		# Venn diagrams.
		echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\centering' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"$datasetName"'_venn_FemaleVsMale_'"$varName"'.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_FemaleVsMale_PD_${varName}.png ]
		then
			echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"$datasetName"'_venn_FemaleVsMale_PD_'"$varName"'.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		fi
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_FemaleVsMale_control_${varName}.png ]
		then
			echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"$datasetName"'_venn_FemaleVsMale_control_'"$varName"'.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		fi
		echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"$datasetName"'_venn_PDVsControl_'"$varName"'.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_PDVsControl_females_${varName}.png ]
		then
			echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"$datasetName"'_venn_PDVsControl_females_'"$varName"'.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		fi
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_PDVsControl_males_${varName}.png ]
		then
			echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"$datasetName"'_venn_PDVsControl_males_'"$varName"'.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		fi
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_Gender_disease_status_${varName}.png ]
		then
			echo '	\includegraphics[scale=0.23]{'"$OUTPUT_FOLDER"''"$datasetName"'_venn_Gender_disease_status_'"$varName"'.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		fi
		echo '	\caption{Venn diagrams of the significant genes for all Limma analyses and for dataset '"$datasetName"' ('"$varName"' data).' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	From top to bottom and from left to right: ' >> ${OUTPUT_FOLDER}results_summary.tex
		p=1
		echo '	('"$p"') Gender based analysis (Females vs males)' >> ${OUTPUT_FOLDER}results_summary.tex
		p=$((p+1))
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_FemaleVsMale_PD_${varName}.png ]
		then
			echo '	('"$p"') Gender based analysis (Females vs males) in PD patients.' >> ${OUTPUT_FOLDER}results_summary.tex
			p=$((p+1))
		fi
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_FemaleVsMale_control_${varName}.png ]
		then
			echo '	('"$p"') Gender based analysis (Females vs males) in controls.' >> ${OUTPUT_FOLDER}results_summary.tex
			p=$((p+1))
		fi
		echo '	('"$p"') Status based analysis (PD patients vs controls).' >> ${OUTPUT_FOLDER}results_summary.tex
		p=$((p+1))
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_PDVsControl_females_${varName}.png ]
		then
			echo '	('"$p"') Status based analysis (PD patients vs controls) for females.' >> ${OUTPUT_FOLDER}results_summary.tex
			p=$((p+1))
		fi
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_PDVsControl_males_${varName}.png ]
		then
			echo '	('"$p"') Status based analysis (PD patients vs controls) for males.' >> ${OUTPUT_FOLDER}results_summary.tex
			p=$((p+1))
		fi
		if [ -f ${OUTPUT_FOLDER}${datasetName}_MD_Gender_disease_status_${varName}.png ]
		then
			echo '	('"$p"') Factorial analysis (focus on disease status differences for the different genders).' >> ${OUTPUT_FOLDER}results_summary.tex
			p=$((p+1))
		fi
		echo '	}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex
	done
	echo '\clearpage' >> ${OUTPUT_FOLDER}results_summary.tex
done

echo '\clearpage' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex

# Per biomarker analysis.
nbBiomarkers=${#biomarkers__name[@]}
for (( i=0; i<$nbBiomarkers; i++ ))
do
	biomarkerName=${biomarkers__name[$i]}
	biomarkerTissue=${biomarkers__tissue[$i]}
	biomarkerTissueForFile=${biomarkerTissue/ /_}
	echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	\centering' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	\includegraphics[scale=0.43]{'"$OUTPUT_FOLDER"'Biomarker_'"${biomarkerTissueForFile}"'_'"${biomarkerName}"'.png}' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	\caption{Expression values for biomarker '"${biomarkerName}"' for both DA and SN tissues.' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	(Top) Usage of expression values as derived from pre-processing. (Bottom) Usage of Z-scores ' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '	derived from the expression values. (Left) VSN correction applied. (Right) No VSN correction applied.}' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
	echo '' >> ${OUTPUT_FOLDER}results_summary.tex
done

# Print footer
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\end{document}' >> ${OUTPUT_FOLDER}results_summary.tex

# Compilation
pdflatex -synctex=1 -interaction=nonstopmode ${OUTPUT_FOLDER}results_summary.tex
mv results_summary.pdf ${OUTPUT_FOLDER}
rm results_summary*

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
