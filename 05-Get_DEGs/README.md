# Objectives
The objectives of this step is to identify the differentially expressed genes of each dataset, and for various comparisons of interest (e.g., female vs male, disease versus control). This produces gene lists that can be then used for the pathway and network analyses.

# Details and instructions
The datasets are processed one by one to identify differentially expressed genes (using limma). The following analyses are performed:
- Females vs males
- PD patients vs controls
- Female PD patients vs female controls
- Male PD patients vs male controls
- (Female PD patients vs female controls) vs (Male PD patients vs male controls)
- Female patients vs male patients
- Female controls vs male controls
- (Female patients vs male patients) vs (Female controls vs male controls) [equivalent to #5]

Various plots and lists are created in the process. The results are summarized at the probe level but with gene annotations.
```
make clean_outputs
make run_limma
```

Then, the expression levels of the various biomarkers are checked.
```
make biomarker
```

A document that contains all figures can then be generated.
```
make doc
```

Notice that not all datasets are analyzed using the exact same method. The co-factors might be different depending on whether there are replicates, potential batch effects, and complete clinical annotations (such as age). In addition, some datasets do not have enough samples in a given category (for instance, female PD patients) to perform the complex analyses that take this category into account. This means that for some datasets not all analyses are performed.

# Prerequisites
The only prerequisite is to have the preprocessed and cleaned data for all datasets (Step 04).