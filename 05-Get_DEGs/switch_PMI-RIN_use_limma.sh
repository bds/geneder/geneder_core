#!/bin/bash -l

# Defining global parameters.
INPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/04/
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/05/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/05-Get_DEGs/

NBC=$(ls ${OUTPUT_FOLDER}*cortable_PMI*   | wc -l)
NBL=$(ls ${OUTPUT_FOLDER}*limmatable_PMI* | wc -l)

# We only move files when we are in the correct situation
# (we have *cortable* files but no *limmatable* files).
if [ "$NBL" -gt "0" ]; then
  if [ "$NBC" -eq "0" ]; then

    # We move all existing toptable to cortable (as backup).
    echo "[PMI] We can move the files (correct configuration) [c = $NBC // l = $NBL]"
    FILES=${OUTPUT_FOLDER}*toptable_PMI*
    for f in $FILES
    do
      f2=${f/toptable/cortable}
      mv $f $f2
    done

    # We move all existing limmatable to toptable (to be used in the meta-analysis).
    FILES=${OUTPUT_FOLDER}*limmatable_PMI*
    for f in $FILES
    do
      f2=${f/limmatable/toptable}
      mv $f $f2
    done
    echo "[PMI] Files moved."
  else
    echo "[PMI] We can not move the files (incorrect configuration) [c = $NBC // l = $NBL]"
  fi
else
  echo "[PMI] We can not move the files (incorrect configuration) [c = $NBC // l = $NBL]"
fi

NBC=$(ls ${OUTPUT_FOLDER}*cortable_RIN*   | wc -l)
NBL=$(ls ${OUTPUT_FOLDER}*limmatable_RIN* | wc -l)

# We only move files when we are in the correct situation
# (we have *cortable* files but no *limmatable* files).
if [ "$NBL" -gt "0" ]; then
  if [ "$NBC" -eq "0" ]; then

    # We move all existing toptable to cortable (as backup).
    echo "[RIN] We can move the files (correct configuration) [c = $NBC // l = $NBL]"
    FILES=${OUTPUT_FOLDER}*toptable_RIN*
    for f in $FILES
    do
      f2=${f/toptable/cortable}
      mv $f $f2
    done

    # We move all existing limmatable to toptable (to be used in the meta-analysis).
    FILES=${OUTPUT_FOLDER}*limmatable_RIN*
    for f in $FILES
    do
      f2=${f/limmatable/toptable}
      mv $f $f2
    done
    echo "[RIN] Files moved."
  else
    echo "[RIN] We can not move the files (incorrect configuration) [c = $NBC // l = $NBL]"
  fi
else
  echo "[RIN] We can not move the files (incorrect configuration) [c = $NBC // l = $NBL]"
fi
