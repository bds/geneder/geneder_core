#!/usr/bin/env Rscript

# ================================================================================================
# Libraries
# ================================================================================================
library("yaml")
library("stringr")
library("org.Hs.eg.db")
library("tidyverse")
library("ReactomePA")
library("DOSE")
library("clusterProfiler")
library("msigdbr")
source("../libs/conf/confR.R")
source("../libs/utils/utils.R")
message(paste0("[", Sys.time(), "] Libraries loaded."))

# ================================================================================================
# Configuration
# ================================================================================================
options(bitmapType = "cairo")
config          <- read_config(config_dirs = c("../Confs/", "./"))
raw_data_dir    <- config$global_raw_data_dir
output_data_dir <- paste0(config$global_data_dir, config$local_data_dir)
input_data_dir  <- paste0(config$global_data_dir, config$local_input_data_dir)

# Optional command line parameter to process only one dataset.
args <- commandArgs(trailingOnly = TRUE)
selected_dataset_name <- ""
if (length(args) > 0) {
  selected_dataset_name <- args[1]
}
rm(args)

# Configure MSigDB
msigdb <- msigdbr(species = "Homo sapiens") %>% dplyr::select(gs_name, entrez_gene)
message(paste0("[", Sys.time(), "] Configuration done."))

# ================================================================================================
# Functions
# ================================================================================================

#' @title Perform a regular enrichment analysis using Fisher's over-representation.
#'
#' @description This functions relies on clusterProfiler and the associated packages
#' to run the enrichment for a given ontology among DO, GO (all three subtypes), KEGG and
#' REACTOME. It saves the results in the provided folder and return the enrichResult object
#' for further plotting.
#'
#' @param genes The list of genes to analyse. The gene identifiers have to match what is
#' expected - be it entrezgene identifiers or gene symbols.
#' @param  background The list of all genes to be used as a background for correct
#' estimation of the statistics. The genes identifiers also have to match what is expected.
#' @param type The name of the ontology to use among "DO", "GOMF", "GOBP", "GOCC",
#' "KEGG" and "REACTOME". The GO analyses relies on gene symbols, the others use entrezgene ids.
#' Default to "DO".
#' @param file_dir The folder in which the tables will be saved. Default to "".
#' @param file_prefix The string that can be used as a prefix to name the file. Default to "".
#' @param symplify A boolean indicating whether the results should be simplified (by removing
#' redundant terms). Default to TRUE.
#' @return The enrichResult object that contains the enriched terms.
perform_enrichment <- function(genes,
                               background,
                               type        = "DO",
                               file_dir    = "",
                               file_prefix = "",
                               simplify    = TRUE) {

  # Default case, we just set the result to NULL.
  enrich <- NULL
  switch(type,
         DO = {
           enrich <- DOSE::enrichDO(gene          = genes,
                                    universe      = background,
                                    ont           = "DO",
                                    minGSSize     = config$min_gs_size,
                                    maxGSSize     = config$max_gs_size,
                                    pvalueCutoff  = 1,
                                    qvalueCutoff  = 1,
                                    pAdjustMethod = "BH",
                                    readable      = TRUE)
         },
         GOMF = {
           enrich <- clusterProfiler::enrichGO(gene          = genes,
                                               universe      = background,
                                               keyType       = "SYMBOL",
                                               OrgDb         = org.Hs.eg.db, #nolint
                                               ont           = "MF",
                                               minGSSize     = 10,
                                               maxGSSize     = config$max_gs_size,
                                               pvalueCutoff  = 1,
                                               qvalueCutoff  = 1,
                                               pAdjustMethod = "BH",
                                               readable      = FALSE)
         },
         GOBP = {
           enrich <- clusterProfiler::enrichGO(gene          = genes,
                                               universe      = background,
                                               keyType       = "SYMBOL",
                                               OrgDb         = org.Hs.eg.db, #nolint
                                               ont           = "BP",
                                               minGSSize     = config$min_gs_size,
                                               maxGSSize     = config$max_gs_size,
                                               pvalueCutoff  = 1,
                                               qvalueCutoff  = 1,
                                               pAdjustMethod = "BH",
                                               readable      = FALSE)
         },
         GOCC = {
           enrich <- clusterProfiler::enrichGO(gene          = genes,
                                               universe      = background,
                                               keyType       = "SYMBOL",
                                               OrgDb         = org.Hs.eg.db, #nolint
                                               ont           = "CC",
                                               minGSSize     = config$min_gs_size,
                                               maxGSSize     = config$max_gs_size,
                                               pvalueCutoff  = 1,
                                               qvalueCutoff  = 1,
                                               pAdjustMethod = "BH",
                                               readable      = FALSE)
         },
         KEGG = {
           enrich <- clusterProfiler::enrichKEGG(gene              = genes,
                                                 universe          = background,
                                                 organism          = "hsa",
                                                 minGSSize         = config$min_gs_size,
                                                 maxGSSize         = config$max_gs_size,
                                                 pvalueCutoff      = 1,
                                                 qvalueCutoff      = 1,
                                                 pAdjustMethod     = "BH",
                                                 use_internal_data = FALSE)
         },
         REACTOME = {
           enrich <- ReactomePA::enrichPathway(gene          = genes,
                                               universe      = background,
                                               organism      = "human",
                                               minGSSize     = config$min_gs_size,
                                               maxGSSize     = config$max_gs_size,
                                               pvalueCutoff  = 1,
                                               qvalueCutoff  = 1,
                                               pAdjustMethod = "BH",
                                               readable      = TRUE)
         }
  )

  # We simplify the enrichment results by removing the potentially redundant terms.
  if (simplify == TRUE) {
    enrich <- clusterProfiler::simplify(enrich,
                                        cutoff     = 0.75,
                                        by         = "p.adjust",
                                        select_fun = min)
  }

  # We save the table as TSV file.
  enrich_fn <- paste0(file_dir, file_prefix, "enrich_", type, ".tsv")
  write.table(enrich, file = enrich_fn, sep = "\t", quote = FALSE, col.names = NA)

  # We return the enrichResult object.
  return(enrich)
}

# ================================================================================================
# Main
# ================================================================================================

# We do all datasets one by one.
for (i in seq_len(length(config$scdatasets))) {

  # We get the dataset details.
  dataset      <- config$scdatasets[[i]]
  dataset_name <- dataset$dataset_name

  # We run the quality control of the current dataset (if necessary).
  if (selected_dataset_name == "" || selected_dataset_name == dataset_name) {

    # We define the I/Os
    raw_data_subdir    <- paste0(raw_data_dir,    dataset_name, "/data/")
    output_data_subdir <- paste0(output_data_dir, dataset_name, "_seurat/")

    # We are loading the background
    background_fn <- paste0(raw_data_subdir, "genes.tsv")
    background    <- read.table(background_fn,
                                header    = FALSE,
                                sep       = "\t",
                                row.names = NULL,
                                as.is     = TRUE)$V2
    rm(background_fn)

    # We map gene symbols to entrezgene ids for background.
    background_map <- bitr(background,
                           fromType = "SYMBOL",
                           toType   = c("ENTREZID"),
                           OrgDb    = org.Hs.eg.db) %>% #nolint
      filter(SYMBOL %in% background)
    rm(background)

    # We perform the enrichment on all rankings files.
    ranking_files <- list.files(path = output_data_subdir, pattern = "*rankings.tsv")

    # We do all integration schemes one by one.
    for (ranking_file in ranking_files) {

      # We define the I/Os.
      analysis_prefix <- strsplit(ranking_file, ".tsv")[[1]]
      ofile_prefix    <- paste0("CP_", analysis_prefix, "_")
      input_filename  <- paste0(output_data_subdir, ranking_file)
      message(paste0("[", Sys.time(), "][", analysis_prefix, "] CP analysis started."))

      # We read the prepared ranking and keep only the value used to rank.
      ranking <- read.delim(input_filename, stringsAsFactors = FALSE) %>%
        select(Gene, adj_P_value)
      rm(input_filename)
      ranking_clean <- ranking
      ranking_clean$adj_P_value     <- as.numeric(ranking_clean$adj_P_value)
      rm(ranking)
      message(paste0("[", Sys.time(), "][", analysis_prefix, "] Data loaded (",
                     dim(ranking_clean)[1], " genes)."))

      # We map the gene symbols to the EntrezGene identifiers.
      mapped_egenes <- tryCatch(
        expr = {
          bitr(ranking_clean$Gene,
               fromType = "SYMBOL",
               toType   = c("ENTREZID"),
               OrgDb    = org.Hs.eg.db) #nolint
        },
        error = function(e) {
          message(paste0("[", Sys.time(), "][", analysis_prefix,
                         "] Error in gene identifier mapping:"))
          message(e)
        }
      )
      if (is.null(mapped_egenes)) {
        rm(analysis_prefix, ofile_prefix, ranking_clean, mapped_egenes)
        next
      }
      ranking_final <- merge(x       = ranking_clean,
                             y       = mapped_egenes,
                             by.x    = "Gene",
                             by.y    = "SYMBOL",
                             all.x   = TRUE) %>%
        mutate(EGene = ENTREZID) %>%
        select(Gene, EGene, adj_P_value)
      ranking_final$adj_P_value     <- as.numeric(ranking_final$adj_P_value)
      ranking_final                 <- ranking_final %>% arrange(adj_P_value)
      ranking_final_eg              <- ranking_final %>% filter(!is.na(EGene))
      rm(mapped_egenes, ranking_clean)
      message(paste0("[", Sys.time(), "][", analysis_prefix, "] Data mapped (",
                     dim(ranking_final_eg)[1], "/", dim(ranking_final)[1], " genes)."))

      # We then prepare the gene lists for Fisher-based enrichment. These are just gene lists based
      # on the adjusted P values.
      selection_sy <- ranking_final %>% filter(adj_P_value < 0.05)
      selection_eg <- selection_sy  %>% filter(!is.na(EGene))
      rm(ranking_final, ranking_final_eg)
      message(paste0("[", Sys.time(), "][", analysis_prefix, "] Data filtered (",
                     dim(selection_eg)[1], "/", dim(selection_sy)[1], " genes)."))

      # We loop over the functional sources.
      for (k in seq_len(length(config$functional_sources))) {

        # We define the current source.
        func_source <- config$functional_sources[[k]]
        if (is.null(func_source$cp_name) || func_source$cp_name == "") {
          next
        }

        # We do not do the MSIGDB enrichment (was only for GSEA).
        if (func_source$cp_name == "MSIGDB") {
          next
        }

        # We define the reference genes (depending on the source).
        selection  <- NULL
        background <- NULL
        if (func_source$cp_gene_id_type == "entrezgene") {
          selection  <- selection_eg$EGene
          background <- background_map$ENTREZID
        } else if (func_source$cp_gene_id_type == "symbol") {
          selection  <- selection_sy$Gene
          background <- background_map$SYMBOL
        }

        # We run the Fisher analysis.
        fisher_adjP <- perform_enrichment(selection,
                                          background,
                                          type        = func_source$cp_name,
                                          file_dir    = output_data_subdir,
                                          file_prefix = ofile_prefix,
                                          simplify    = FALSE)

        message(paste0("[", Sys.time(), "][", analysis_prefix, "] CP enrichment (",
                       func_source$cp_name, ") done."))
        rm(func_source, fisher_adjP, selection, background)
      } # End for each functional source.
      message(paste0("[", Sys.time(), "][", analysis_prefix, "] CP analysis done."))
      rm(k, selection_sy, selection_eg)
      rm(analysis_prefix, ofile_prefix)
    } # End for each integration.
    rm(ranking_file, ranking_files, output_data_subdir, raw_data_subdir, background_map)
  } # End if dataset should be run.
  rm(dataset, dataset_name)
} # End for each dataset
rm(i)

# We log the session details for reproducibility.
rm(config, input_data_dir, output_data_dir, msigdb)
sessionInfo()
