#!/bin/bash -l

# Defining global parameters.
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/09/

# We get the Ensembl and HGNC data
wget -O ${OUTPUT_FOLDER}mitochondrial_genes.tsv 'http://www.ensembl.org/biomart/martservice?query=<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE Query><Query  virtualSchemaName = "default" formatter = "TSV" header = "0" uniqueRows = "0" count = "" datasetConfigVersion = "0.6" ><Dataset name = "hsapiens_gene_ensembl" interface = "default" ><Filter name = "chromosome_name" value = "MT"/><Attribute name = "ensembl_gene_id" /></Dataset></Query>'
wget -O ${OUTPUT_FOLDER}ribosomalrna_genes.tsv  'https://www.genenames.org/cgi-bin/genegroup/download?id=848&type=branch' --no-check-certificate
