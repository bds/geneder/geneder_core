#!/bin/bash -l
#SBATCH -J geneder:09:cp_enrich
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH --time=0-09:00:00
#SBATCH --mem=10GB
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/09/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/09-Single-cell_analysis/
REF=${1}

# Loading modules.
module load lang/R/4.0.5-foss-2020b

# Load configuration
source ../libs/conf/confSH.sh
create_variables ../Confs/datasets_config.yml

# For all scRNA datasets
nbScDatasets=${#scdatasets__dataset_name[@]}
for (( i=0; i<$nbScDatasets; i++ ))
do
	datasetName=${scdatasets__dataset_name[$i]}
	if [ "${datasetName}" == "${REF}" ]
	then
		# We run the functional enrichment.
		echo "== Job $i started (${datasetName}) =="
		# Not used since Seurat outputs mostly DEGs and only a few are not significant so background will be more or less equal to DEGs...
		# We instead rely on the gene list provided with the raw data.
		#cut -f 6 ${OUTPUT_FOLDER}${datasetName}_seurat/DEG_*_*_PDvsCTRL_*_corrected.tsv | sort -u | grep -v '^$$' | sed -r 's/\|/\n/g' | sort -u > ${OUTPUT_FOLDER}${datasetName}_seurat/background.tsv
		Rscript --vanilla ${CODE_FOLDER}/CP_enrich_seurat.R ${datasetName} > ${OUTPUT_FOLDER}/${datasetName}_seurat/CP_enrich_log.out 2> ${OUTPUT_FOLDER}/${datasetName}_seurat/CP_enrich_log.err
		echo "== Job $i ended (${datasetName}) =="
	fi
done

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
