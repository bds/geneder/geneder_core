#!/bin/bash -l
#SBATCH -J geneder:09:rankings
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --time=0-00:10:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/09/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/09-Single-cell_analysis/
REF=${1}

# Loading modules.
resif-load-swset-legacy
module load lang/R/3.6.2-foss-2019b-bare

# Load configuration
source ../libs/conf/confSH.sh
create_variables ../Confs/datasets_config.yml

# For all scRNA datasets
nbScDatasets=${#scdatasets__dataset_name[@]}
for (( i=0; i<$nbScDatasets; i++ ))
do
	datasetName=${scdatasets__dataset_name[$i]}
	if [ "${datasetName}" == "${REF}" ]
	then
		# We compute the gender specificity scores.
		echo "== Job $i started (${datasetName}) =="
		Rscript --vanilla ${CODE_FOLDER}/refine_FDR_values_seurat.R                         > ${OUTPUT_FOLDER}${datasetName}_seurat/refine_FDR_log_seurat.out      2> ${OUTPUT_FOLDER}${datasetName}_seurat/refine_FDR_log_seurat.err
		Rscript --vanilla ${CODE_FOLDER}/compute_gender_specificity_seurat.R ${datasetName} > ${OUTPUT_FOLDER}${datasetName}_seurat/gdr_spec_log_seurat.out        2> ${OUTPUT_FOLDER}${datasetName}_seurat/gdr_spec_log_seurat.err
		Rscript --vanilla ${CODE_FOLDER}/merge_and_filter_rankings_seurat.R ${datasetName}  > ${OUTPUT_FOLDER}${datasetName}_seurat/merge_filter_log_seurat.out    2> ${OUTPUT_FOLDER}${datasetName}_seurat/merge_filter_log_seurat.err
		Rscript --vanilla ${CODE_FOLDER}/refine_GD_rankings_seurat.R ${datasetName}         > ${OUTPUT_FOLDER}${datasetName}_seurat/refine_rankings_log_seurat.out 2> ${OUTPUT_FOLDER}${datasetName}_seurat/refine_rankings_log_seurat.err
		echo "== Job $i ended (${datasetName}) =="
	fi
done

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
