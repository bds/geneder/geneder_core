#!/bin/bash -l
#SBATCH -J geneder:09:seurat
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH --ntasks-per-socket=14
#SBATCH --ntasks-per-node=28
#SBATCH -c 1
#SBATCH --mem=64GB
#SBATCH --time=0-04:00:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/09/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/09-Single-cell_analysis/
REF=${1}

# Loading modules.
resif-load-swset-legacy
module load lang/R/3.6.2-foss-2019b-bare

# Load configuration
source ../libs/conf/confSH.sh
create_variables ../Confs/datasets_config.yml

# For all scRNA datasets
nbScDatasets=${#scdatasets__dataset_name[@]}
for (( i=0; i<$nbScDatasets; i++ ))
do
	datasetName=${scdatasets__dataset_name[$i]}
	if [ "${datasetName}" == "${REF}" ]
	then
		echo "== Job $i started (${datasetName}) =="
		rm -rf ${OUTPUT_FOLDER}${datasetName}_seurat/
		mkdir ${OUTPUT_FOLDER}${datasetName}_seurat/
		Rscript --vanilla ${CODE_FOLDER}analyse_seurat.R ${datasetName} > ${OUTPUT_FOLDER}${datasetName}_seurat/analysis_log.out 2> ${OUTPUT_FOLDER}${datasetName}_seurat/analysis_log.err
		echo "== Job $i ended (${datasetName}) =="
	fi
done

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
