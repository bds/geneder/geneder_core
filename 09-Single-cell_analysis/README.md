# Objectives
The objectives of this step is to analyse the single-cell data (PD versus control) for male and female samples. This step includes the quality control and data pre-processing as well as the differential expression and the functional enrichment. Due to the low number of datasets (*i.e.*, only two), there is no meta-analysis.

# Details and instructions
Note that Seurat version we use relies on the R package spatstat v1.64 and not the latest v2.x.

The single-cell datasets are analysed using a Seurat pipeline.
```
make clean_outputs
make ana
```

At this stage, the datasets have been pre-processed and the diffeential genes have been identified.
for each dataset, the sex-specific and sex-dimorphic genes are first identified and then used to perform functional enrichment using Fisher method (GSEA can not be used as the differential expression analyses do not provide whole genome rankings).
```
make prep
make enrich
```

# Prerequisites
A prerequisite is to have the scRNA data ready in the project data folder and the configutration file setup correctly (as for the bulk transcriptomics data in step 01).