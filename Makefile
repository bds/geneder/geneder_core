FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/
ANNEX=/home/users/ltranchevent/Projects/GeneDER/Documents/WorkReport/Annexes_p3/

clean:
	@rm -rf *~
code_loc_hpc:
	@rsync -vazur --delete /home/leon/Projects/GeneDER/Analysis/ iris:/home/users/ltranchevent/Projects/GeneDER/Analysis/
code_hpc_loc:
	@rsync -vazur --delete iris:/home/users/ltranchevent/Projects/GeneDER/Analysis/ /home/leon/Projects/GeneDER/Analysis/
lib_loc_hpc:
	@rsync -vazur --delete /home/leon/Projects/Rlibs/ iris:/home/users/ltranchevent/Projects/Rlibs/
lib_hpc_loc:
	@rsync -vazur --delete iris:/home/users/ltranchevent/Projects/Rlibs/ /home/leon/Projects/Rlibs/
data_loc_hpc:
	@rsync -vazur          /home/leon/Data/GeneDER/Analysis/ iris:/home/users/ltranchevent/Data/GeneDER/Analysis/
	@rsync -vazur          /home/leon/Data/GeneDER/Original/ iris:/home/users/ltranchevent/Data/GeneDER/Original/
data_loc_eld:
	@rsync -vazur          /home/leon/Data/GeneDER/Analysis/ elrond:/home/users/ltranchevent/Data/GeneDER/Analysis/
	@rsync -vazur          /home/leon/Data/GeneDER/Original/ elrond:/home/users/ltranchevent/Data/GeneDER/Original/
data_hpc_loc:
	@rsync -vazur          iris:/home/users/ltranchevent/Data/GeneDER/Analysis/ /home/leon/Data/GeneDER/Analysis/
	@rsync -vazur          iris:/home/users/ltranchevent/Data/GeneDER/Original/ /home/leon/Data/GeneDER/Original/
install_lib:
	#ii
	@module load lang/R/3.6.2-foss-2019b-bare
	@Rscript --vanilla ${FOLDER}/libs/utils/install.R
up_annex:
	@cp ${OUTPUT_FOLDER}/02/results_summary.pdf ${ANNEX}/02_summary_results.pdf
	@cp ${OUTPUT_FOLDER}/03/results_summary.pdf ${ANNEX}/03_summary_results.pdf
	@cp ${OUTPUT_FOLDER}/04/results_summary.pdf ${ANNEX}/04_summary_results.pdf
	@cp ${OUTPUT_FOLDER}/05/results_summary.pdf ${ANNEX}/05_summary_results.pdf
	@cp ${OUTPUT_FOLDER}/06/results_summary_a.pdf ${ANNEX}/06a_summary_results.pdf
	@cp ${OUTPUT_FOLDER}/06/results_summary_b.pdf ${ANNEX}/06b_summary_results.pdf
	@cp ${OUTPUT_FOLDER}/14/results_summary.pdf ${ANNEX}/14_summary_results.pdf
	@cp ${OUTPUT_FOLDER}/15/results_summary.pdf ${ANNEX}/15_summary_results.pdf
	@cp ${OUTPUT_FOLDER}/16/results_summary_a.pdf ${ANNEX}/16a_summary_results.pdf
	@cp ${OUTPUT_FOLDER}/16/results_summary_b.pdf ${ANNEX}/16b_summary_results.pdf
