# Objectives
The objectives of this step are to predict the gender /age of the patients whose gender / age is not indicated in the clinical annotations.

# Details and instructions
All datasets are used regardless of whether there exists samples with missing clinical annotations. This is motivated by the fact that we also want to estimate the overall accuracy of the predictions. The Makefile contains the commands to get the data from Biomart and then make the predictions. Plots are made and whether the predicted data should be used is left to the user to decide (manually).
```
make clean_outputs
make predict
```

A PDF document that contains all the figures is then created for manual inspection.
```
make doc
```

# Prerequisites
The prerequisites are to have the raw data (mostly from GEO) in the Data folder and the pre-processed data from step 02. There should be one folder per dataset, with a '/RAW/' folder with the raw data as CEL files (array data) or a TSV file with the pre-processed data (RNA-seq). In addition , a "ClinicalData.tsv" file should contain the clinical annotations (including of course gender and age).