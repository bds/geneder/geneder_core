#!/bin/bash -l
#SBATCH -J geneder:03:doc
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --time=0-00:01:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# I/Os and parameters
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/03/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/03-Predict_missing/

# Load configuration
source ../libs/conf/confSH.sh
create_variables ../Confs/datasets_config.yml
create_variables ../Confs/project_config.yml

# Clean start
rm -rf ${OUTPUT_FOLDER}results_summary.*

# Print header
echo '\documentclass[]{article}' > ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\usepackage{graphicx}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\title{GeneDER - step 03 - Gender prediction}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\author{Leon-Charles Tranchevent}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\begin{document}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\maketitle' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\textsl{}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\begin{abstract}' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'This document summarizes the results of the step 03-Predict\_missing, in which gender and age missing values are predicted.' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'For age, the scatter plot of the real versus predicted values is displayed. For gender, heatmaps of the expression' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'signal of Y-chromosome probes are displayed for all datasets. In addition, clustering results are displayed on top of' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'the heatmaps to see if samples with shared gender are clustered together. Last, signal from' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'Y-chromosome probes versus signal from X-chromosome probes is are also plotted. Blue samples are' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'females, green are males, grey are unknown (missing data - to be predicted). \\' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo 'Note: this document is automatically generated.' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\end{abstract}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex

# Age prediction.
echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
echo '	\centering' >> ${OUTPUT_FOLDER}results_summary.tex
echo '	\includegraphics[scale=0.32]{'"$OUTPUT_FOLDER"'global_age_predictions.png}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '	\caption{Scatter plots (per dataset) of the known age versus the predicted age.' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
echo '	Lines indicate the perfect match (solid line) or the 5- and 10-year intervals (dashed lines).}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
echo '' >> ${OUTPUT_FOLDER}results_summary.tex

# Gender prediction.
nbDatasets=${#datasets__dataset_name[@]}
for (( i=0; i<$nbDatasets; i++ ))
do
	datasetName=${datasets__dataset_name[$i]}

	# For each normalization method.
	nbNorms=${#normalizations__name[@]}
	for (( j=0; j<$nbNorms; j++ ))
	do
		normName=${normalizations__name[$j]}

		# By default, first no batch correction.
		echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\centering' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\includegraphics[scale=0.32]{'"$OUTPUT_FOLDER"''"${datasetName}"'/'"${datasetName}"'_'"${normName}"'_nobatchcorrection_heatmap_yprobes.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '  \includegraphics[scale=0.32]{'"$OUTPUT_FOLDER"''"${datasetName}"'/'"${datasetName}"'_'"${normName}"'_nobatchcorrection_plot_YvsX.png}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	\caption{Plots for the '"$datasetName"' dataset ('"${normName}"', no batch correction). (Left) Heatmap for all Y-chromosome probes.' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	(Right) Plot of the ratio between the average Y and average X signals.' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '	Blue is for females, green for males and grey for unknown gender.}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
		echo '' >> ${OUTPUT_FOLDER}results_summary.tex

		# If possible, second with batch correction.
		if [ -f  ${OUTPUT_FOLDER}${datasetName}/${datasetName}_${normName}_batchcorrection_heatmap_yprobes.png ]
		then
			echo '\begin{figure}[ht]' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '	\centering' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '	\includegraphics[scale=0.32]{'"$OUTPUT_FOLDER"''"${datasetName}"'/'"${datasetName}"'_'"${normName}"'_batchcorrection_heatmap_yprobes.png}' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '  \includegraphics[scale=0.32]{'"$OUTPUT_FOLDER"''"${datasetName}"'/'"${datasetName}"'_'"${normName}"'_batchcorrection_plot_YvsX.png}' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '	\caption{Plots for the '"$datasetName"' dataset ('"${normName}"', batch correction). (Left) Heatmap for all Y-chromosome probes.' | sed -r 's/_/\\_/g' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '	(Right) Plot of the ratio between the average Y and average X signals.' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '	Blue is for females, green for males and grey for unknown gender.}' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '\end{figure}' >> ${OUTPUT_FOLDER}results_summary.tex
			echo '' >> ${OUTPUT_FOLDER}results_summary.tex
		fi
	done
done

# Print footer
echo '' >> ${OUTPUT_FOLDER}results_summary.tex
echo '\end{document}' >> ${OUTPUT_FOLDER}results_summary.tex

# Compilation
pdflatex -synctex=1 -interaction=nonstopmode ${OUTPUT_FOLDER}results_summary.tex
mv results_summary.pdf ${OUTPUT_FOLDER}
rm results_summary*

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
