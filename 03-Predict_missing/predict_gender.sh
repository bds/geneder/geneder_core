#!/bin/bash -l
#SBATCH -J geneder:03:predGender
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 2
#SBATCH --time=0-00:10:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
GEO_PLATFORM_FOLDER=/home/users/ltranchevent/Data/GeneDER/Original/Platforms/
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/03/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/03-Predict_missing/

# Loading modules.
resif-load-swset-legacy
module load lang/R/3.6.2-foss-2019b-bare

# Load configuration
source ../libs/conf/confSH.sh
create_variables ../Confs/datasets_config.yml
create_variables ../Confs/platforms_config.yml

# Get the biomart data.
nbPlatforms=${#platforms__platform_name[@]}
for (( i=0; i<$nbPlatforms; i++ ))
do
	platformName=${platforms__platform_name[$i]}
	platformBiomartName=${platforms__biomart_name[$i]}
	platformGEOName=${platforms__geo_name[$i]}
	if [ "${platformBiomartName}" != "NA" ]
	then
		# Two runs for the X and Y chromosomes.
		wget -O ${OUTPUT_FOLDER}${platformName}_chromX.tsv 'http://www.ensembl.org/biomart/martservice?query=<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE Query><Query  virtualSchemaName = "default" formatter = "TSV" header = "0" uniqueRows = "1" count = "" datasetConfigVersion = "0.6"><Dataset name = "hsapiens_gene_ensembl" interface = "default"><Filter name = "chromosome_name" value = "X"/><Attribute name = "'${platformBiomartName}'"/></Dataset></Query>'
		sleep 2s
		wget -O ${OUTPUT_FOLDER}${platformName}_chromY.tsv 'http://www.ensembl.org/biomart/martservice?query=<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE Query><Query  virtualSchemaName = "default" formatter = "TSV" header = "0" uniqueRows = "1" count = "" datasetConfigVersion = "0.6"><Dataset name = "hsapiens_gene_ensembl" interface = "default"><Filter name = "chromosome_name" value = "Y"/><Attribute name = "'${platformBiomartName}'"/></Dataset></Query>'
		sleep 2s
	else
		if [ "${platformGEOName}" != "NA" ]
		then
			# We use the GEO data
			cut -f 1 ${GEO_PLATFORM_FOLDER}${platformGEOName}_chromX.tsv > ${OUTPUT_FOLDER}${platformName}_chromX.tsv
			cut -f 1 ${GEO_PLATFORM_FOLDER}${platformGEOName}_chromY.tsv > ${OUTPUT_FOLDER}${platformName}_chromY.tsv
		else
			# We use manually curated data.
			cut -f 1 ${GEO_PLATFORM_FOLDER}${platformName}_chromX.tsv > ${OUTPUT_FOLDER}${platformName}_chromX.tsv
			cut -f 1 ${GEO_PLATFORM_FOLDER}${platformName}_chromY.tsv > ${OUTPUT_FOLDER}${platformName}_chromY.tsv
		fi
	fi
done

# Run the gender prediction jobs.
nbDatasets=${#datasets__dataset_name[@]}
for (( i=0; i<$nbDatasets; i++ ))
do
	datasetName=${datasets__dataset_name[$i]}
	mkdir ${OUTPUT_FOLDER}${datasetName}
	Rscript --vanilla ${CODE_FOLDER}predict_gender.R ${datasetName} > ${OUTPUT_FOLDER}${datasetName}/${datasetName}_genderpredictions.out 2> ${OUTPUT_FOLDER}${datasetName}/${datasetName}_genderpredictions.err
done

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
