# Objectives
The objectives of this step are to analyze the quality of the raw data and to create reports that can be visually inspected to decide which samples should be kept and which should be discarded.

# Details and instructions
Affymetrix datasets are analysed using R and the 'arrayQualityMetrics' package, which produces HTML reports with figures and descriptions. Agilent datasets are analysed using home-made scripts, which creates figures but does not summarize the QC in a single document. The Makefile contains the commands to launch all jobs. 
```
make clean_outputs
make run_qc
```

# Prerequisites
Since this is the first step of the analysis, the only prerequisite is to have the raw data (mostly from GEO) in the Data folder. There should be one folder per dataset, with a TSV file containing the clinical data ('ClinicalData.tsv' and a '/RAW/' folder with the raw data (should not be compressed unless a GEO series matrix file).