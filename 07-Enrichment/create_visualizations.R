#!/usr/bin/env Rscript

# ================================================================================================
# Libraries
# ================================================================================================
library("yaml")
library("org.Hs.eg.db")
library("tidyverse")
library("clusterProfiler")
library("pathview")
source("../libs/conf/confR.R")
source("../libs/utils/utils.R")
message(paste0("[", Sys.time(), "] Libraries loaded."))

# ================================================================================================
# Configuration
# ================================================================================================
options(bitmapType = "cairo")
config          <- read_config(config_dirs = c("../Confs/", "./"))
output_data_dir <- paste0(config$global_data_dir, config$local_data_dir)
input_data_dir  <- paste0(config$global_data_dir, config$local_input_data_dir)
message(paste0("[", Sys.time(), "] Configuration done."))

# ================================================================================================
# Functions
# ================================================================================================

# ================================================================================================
# Main
# ================================================================================================

# We define the I/Os.
F_ranking_file <- paste0(output_data_dir,
                         "SNage_PDVsControl_females_max-avg_all_pivalue_rankings.tsv")
M_ranking_file <- paste0(output_data_dir,
                         "SNage_PDVsControl_males_max-avg_all_pivalue_rankings.tsv")

# We read the prepared ranking and keep only the value used to rank.
F_ranking <- read.delim(F_ranking_file, stringsAsFactors = FALSE) %>%
  select(Gene, log_fold_change, adj_P_value)
M_ranking <- read.delim(M_ranking_file, stringsAsFactors = FALSE) %>%
  select(Gene, log_fold_change, adj_P_value)
rm(F_ranking_file, M_ranking_file)

# We map the gene symbols to the EntrezGene identifiers.
F_mapped_egenes <- bitr(F_ranking$Gene,
                        fromType = "SYMBOL",
                        toType   = c("ENTREZID"),
                        OrgDb    = org.Hs.eg.db) #nolint
F_ranking_final <- merge(x     = F_ranking,
                         y     = F_mapped_egenes,
                         by.x  = "Gene",
                         by.y  = "SYMBOL",
                         all.x = TRUE) %>%
  mutate(EGene = ENTREZID) %>%
  select(Gene, EGene, log_fold_change, adj_P_value)
M_mapped_egenes <- bitr(M_ranking$Gene,
                        fromType = "SYMBOL",
                        toType   = c("ENTREZID"),
                        OrgDb    = org.Hs.eg.db) #nolint
M_ranking_final <- merge(x     = M_ranking,
                         y     = M_mapped_egenes,
                         by.x  = "Gene",
                         by.y  = "SYMBOL",
                         all.x = TRUE) %>%
  mutate(EGene = ENTREZID) %>%
  select(Gene, EGene, log_fold_change, adj_P_value)
F_ranking_final$logFC           <- as.numeric(F_ranking_final$log_fold_change)
F_ranking_final$adj_P_value     <- as.numeric(F_ranking_final$adj_P_value)
M_ranking_final$logFC           <- as.numeric(M_ranking_final$log_fold_change)
M_ranking_final$adj_P_value     <- as.numeric(M_ranking_final$adj_P_value)
rm(F_mapped_egenes, M_mapped_egenes)

# We also save the data for GeneGo (manual work on the pathway maps).
FM <- merge(x = F_ranking_final, y = M_ranking_final,
            by = "Gene", all = TRUE, suffixes = c("_F", "_M")) %>%
  select(Gene, logFC_F, adj_P_value_F, logFC_M, adj_P_value_M)
FM_fn <- paste0(output_data_dir, "SNage_PDVsControl_both_max-avg_all_merged_rankings_4GeneGo.tsv")
FM$logFC_F      [is.na(FM$logFC_F)]       <- 0
FM$adj_P_value_F[is.na(FM$adj_P_value_F)] <- 1
FM$logFC_M      [is.na(FM$logFC_M)]       <- 0
FM$adj_P_value_M[is.na(FM$adj_P_value_M)] <- 1
write.table(FM, file = FM_fn, sep = "\t", quote = FALSE, row.names = FALSE)
rm(FM, FM_fn)

# Alternatively, max P = 1
F_logFC_eg          <- F_ranking_final$logFC
names(F_logFC_eg)   <- F_ranking_final$EGene
F_logFC_eg          <- F_logFC_eg[!duplicated(names(F_logFC_eg))]
F_logFC_eg          <- F_logFC_eg[!is.na(names(F_logFC_eg))]
M_logFC_eg          <- M_ranking_final$logFC
names(M_logFC_eg)   <- M_ranking_final$EGene
M_logFC_eg          <- M_logFC_eg[!duplicated(names(M_logFC_eg))]
M_logFC_eg          <- M_logFC_eg[!is.na(names(M_logFC_eg))]
rm(F_ranking_final, M_ranking_final)

# Reformat the data.
FM <- merge(x = data.frame(EGene = names(F_logFC_eg), F_log = F_logFC_eg),
            y = data.frame(EGene = names(M_logFC_eg), M_log = M_logFC_eg),
            by = "EGene", all = TRUE)
row.names(FM) <- FM$EGene
FM$EGene <- NULL
FM$F_log[is.na(FM$F_log)] <- 0
FM$M_log[is.na(FM$M_log)] <- 0
rm(F_logFC_eg, M_logFC_eg)

# Selection of KEGG pathways to be plotted.
my_ids <- c("hsa00020", "hsa00061", "hsa00190", "hsa04060", "hsa04061", "hsa04062", "hsa04668",
            "hsa04721")

for (my_id in my_ids) {
  fig_fn <- paste0(output_data_dir, "SNage_PDVsControl_both_max-avg_PMAP_", my_id, ".png")
  png(fig_fn)
  pv.out <- pathview(gene.data = FM, pathway.id = my_id, species = "hsa",
                     node.sum = "max",
                     bins = list(gene = 20, cpd = 20),
                     out.suffix = "regular-color", low = "green", high = "orange")
  rm(pv.out)
  dev.off()
  rm(fig_fn)
}
rm(my_id, my_ids, FM)

# Clean package objects.
rm(bods, korg)

# We log the session details for reproducibility.
rm(config, output_data_dir, input_data_dir)
sessionInfo()
