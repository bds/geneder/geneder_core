#!/bin/bash -l
#SBATCH -J geneder:07:cp_enrich
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 25
#SBATCH --time=0-03:00:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
INPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/06/
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/07/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/07-Enrichment/

# Loading modules.
module load lang/R/4.0.5-foss-2020b

# Actual jobs
Rscript --vanilla ${CODE_FOLDER}CP_enrich.R > ${OUTPUT_FOLDER}CP_enrich_log.out 2> ${OUTPUT_FOLDER}CP_enrich_log.err

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}
