# Objectives
The objectives of this step is to perform the enrichment analyses of the DEGs.

# Details and instructions
The genes are analyzed to identify functional terms that are enriched (functions, pathways or diseases).
The sex-dimorphic and sex-specific genes are derived from the previous analyses.
```
make clean_outputs
make prepare
```

We then perform the enrichment with ClusterProfiler (self-contained, GSEA).
```
make enrich
```

Then, we also derive the gender specific pathways (gsp) from the enrichment results based on the non specific genes.
```
make gsp
```

Ultimately, we create pathway figures using pathview.
```
make figures
```

# Prerequisites
A prerequisite is to have the results of the integration (step 06).