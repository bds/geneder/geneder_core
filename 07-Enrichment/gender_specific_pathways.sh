#!/bin/bash -l
#SBATCH -J geneder:07:gsp
#SBATCH --mail-type=all
#SBATCH --mail-user=leon-charles.tranchevent@uni.lu
#SBATCH -N 1
#SBATCH -n 8
#SBATCH --time=0-00:01:00
#SBATCH -p batch
#SBATCH --qos=normal

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"
echo ""

# Defining global parameters.
INPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/06/
OUTPUT_FOLDER=/home/users/ltranchevent/Data/GeneDER/Analysis/07/
CODE_FOLDER=/home/users/ltranchevent/Projects/GeneDER/Analysis/07-Enrichment/

# Loading modules.
resif-load-swset-legacy
module load lang/R/3.6.2-foss-2019b-bare

# Actual jobs
Rscript --vanilla ${CODE_FOLDER}/gender_specific_pathways.R            > ${OUTPUT_FOLDER}/gsp_log.out  2> ${OUTPUT_FOLDER}/gsp_log.err
Rscript --vanilla ${CODE_FOLDER}/pathways_with_gender_specific_genes.R > ${OUTPUT_FOLDER}/pgsg_log.out 2> ${OUTPUT_FOLDER}/pgsg_log.err

# Moving the slurm log file to data
mv ${CODE_FOLDER}slurm-${SLURM_JOBID}.out ${OUTPUT_FOLDER}

